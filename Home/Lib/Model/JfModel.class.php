<?php 

/**
* 积分模块
*/
class JfModel extends Model
{
	/**
	 * 添加积分
	 *@param int uid
	 *@param int type
	 */
	public function addJf($uid,$type){
		$jf = 0;
		switch ($type) {
			case '0':
				$jf = 1;
				break;
		case '1':
				$jf = 2;
				break;
		case '2':
				$jf = 3;
				break;
		}
		$data['uid'] = $uid;
		$data['type'] = $type%3;
		$data['jf'] = $jf;
		$data['addtime'] = time();
		$res = M('jf')->add($data);
		return $res==1?true:false;
	}

	/**
	 * 获取用户积分历史记录
	 *@param int uid
	 */
	public function getHis($uid){
		$res = M('jf')->where("uid=$uid")->order('addtime DESC')->select();
		foreach ($res as $key => $value) {
			switch ($value['type']) {
				case '0':
					$res[$key]['msg'] = '点赞';
					break;
				case '1':
					$res[$key]['msg'] = '评论';
					break;
				case '2':
					$res[$key]['msg'] = '分享新闻';
					break;
			}
		}
		return $res;
	}

	public function getTotal($uid){
		$model = M('jf');
		$total = $model->where('uid='.$uid)->field('sum(jf) as jf')->find();
		return is_null($total['jf'])?0:$total['jf'];
	}

}


 ?>