<?php 
/**
* 新闻模型
*/
class NewsModel extends Model
{
	
	protected $_validate=array(
        array('newstitle','require','新闻标题必须',1),
        array('newsdesc','require','新闻简介必须',1),
        array('newsimg','require','新闻缩略图必须',1),
        array('newscontent','require','新闻内容必须',1),
        array('newstag','require','新闻标签必须',1),
    );

    protected $_auto = array (
	    array('addtime','time',1,'function') ,
	);
}



 ?>