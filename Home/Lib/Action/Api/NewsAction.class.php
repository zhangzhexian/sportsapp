<?php 
/**
* 新闻模块
*/
class NewsAction extends CommonAction
{
	
	/**
	 * 获取新闻列表
	 *@param newsid  第一次传0
	 *@param num
	 *@param type  (获取新闻模块时传此参数   0篮球  1足球)
	 *@param matchleagueid (获取联赛新闻时传此参数)
	 */
	public function getNewsList(){
		$newsid = I('newsid',intval);
		$num = I('num',intval);
		$type = I('type',intval);
		$matchleagueid = I('matchleagueid');
		$num = $num==0?5:$num;
		if ($matchleagueid == '') {
			if ($newsid==0) {
				$where = "type=$type and matchleagueid=0";
			}else{
				$where = "newsid<$newsid and type=$type and matchleagueid=0";
			}
		}else{
			if ($newsid==0) {
				$where = "matchleagueid=$matchleagueid";
			}else{
				$where = "newsid<$newsid and matchleagueid=$matchleagueid";
			}
		}
		$res = M('news')->where($where)->limit($num)->order('newsid DESC')->field('newsid,newstitle,newsdesc,newsimg,newstag,addtime')->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$res[$key]['newsimg'] = httpImg($value['newsimg']);
			$res[$key]['commentcount'] = M('newscomment')->where("newsid=".$value['newsid'])->count();
			$res[$key]['shareurl'] = C('host').U('Api/ShareNews/index',array('newsid'=>$value['newsid']));
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取新闻详情
	 *@param newsid
	 */
	public function getNewsInfo(){
		$newsid = I('newsid');
		$res = M('news')->where("newsid=$newsid")->field('newscontent,videourl')->find();
		$res['newscontent'] = imgStrReplace($res['newscontent']);
		get_api_result(200,$res);
	}

	/**
	 * 发布新闻评论
	 *@param uid
	 *@param newsid
	 *@param content
	 */
	public function publishNewsComment(){
		$model = D('Newscomment');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res!=0) {
				$data['newsid'] = $res;
				$ud = formatUser($data['uid']);
				$data['nickname'] = $ud['nickname'];
				$data['headimg'] = $ud['headimg'];
				$data['sex'] = $ud['sex'];
				D('Jf')->addJf($data['uid'],1);
				get_api_result(200,$data);
			}else{
				get_api_result(300,'评论失败');
			}
		}
	}


	/**
	 * 获取评论列表
	 *@param newscommentid 第一次传0
	 *@param num
	 *@param newsid
	 *@param uid 
	 */
	public function getNewsCommentList(){
		$newscommentid = I('newscommentid',intval);
		$num = I('num',intval);
		$uid = I('uid',intval);
		$newsid = I('newsid',intval);
		$num = $num==0?5:$num;
		if ($newscommentid ==0 ) {
			$where = "newsid=$newsid";
		}else{
			$where = "newsid=$newsid and newscommentid<$newscommentid";
		}
		$res = M('newscomment')->where($where)->order('newscommentid DESC')->limit($num)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
			$count = M('newscommentpraise')->where("uid=$uid and newscommentid=".$value['newscommentid'])->count();
			$res[$key]['ispraised'] = $count==0?'no':'yes';
			$res[$key]['praisecount'] = M('newscommentpraise')->where("newscommentid=".$value['newscommentid'])->count();
		}
		get_api_result(200,$res);
	}

	/**
	 * 新闻评论点赞
	 *@param uid
	 *@param newscommentid
	 */
	public function newsCommentPraise(){
		$uid = I('uid');
		$newscommentid = I('newscommentid');
		$count = M('newscommentpraise')->where("uid=$uid and newscommentid=$newscommentid")->count();
		if ($count != 0) {
			get_api_result(302,"您已经点过赞了~");
		}
		$data['uid'] = $uid;
		$data['newscommentid'] = $newscommentid;
		$res = M('newscommentpraise')->add($data);
		if ($res!=0) {
			D('Jf')->addJf($uid,1);
			get_api_result(200,"点赞成功");
		}else{
			get_api_result(300,"点赞失败");
		}
	}

	/**
	 * 获取banner
	 */
	public function getBanner(){
		$res = M('banner')->order('sort')->select();
		foreach ($res as $key => $value) {
			$res[$key]['img'] = httpImg($value['img']);
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取广告详情
	 *@param adid
	 */
	public function getAdInfo(){
		$adid = I('adid',intval);
		$res = M('ad')->where("adid=$adid")->find();
		$res['content'] = imgStrReplace($res['content']);
		get_api_result(200,$res);
	}

	/**
	 * 格式化时间
	 *第二个参数false时为时间戳 
	 */
	private function _getTimeFormat($the_time, $isString)
    {
        if ($isString)
        {
            $the_time = strtotime($the_time);
        }
        $show_time = date("m-d H:i", $the_time);
        $today = strtotime('today');
        $yesterday = $today - 24*3600;
        $durToday = time() - $today;
        $durYesterday = time() - $yesterday;
        $dur = time() - $the_time;
        if($dur < 0){
            return $show_time;
        }
        else
        {
            if($dur < 60)
            {
                return $dur.'秒前';
            }
            else
            {
                if($dur < 3600)
                {
                    return floor($dur/60).'分钟前';
                }
                else
                {
                    if($dur < 10800)
                    {
                        return floor($dur/3600).'小时前';
                    }
                    else
                    {
                        if($dur < $durToday)
                        {//3天内
                            return '今天 '.date('H:i', $the_time);
                        }
                        elseif ($dur < $durYesterday)
                        {
                            return '昨天 '.date('H:i', $the_time);
                        }
                        else
                        {
                            return $show_time;
                        }
                    }
                }
            }
        }
    }

}


 ?>