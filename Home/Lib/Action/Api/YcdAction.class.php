<?php 
/**
* 约场地模块
*/
class YcdAction extends CommonAction
{
	
	/**
	 * 添加场地
	 *@param mixed
	 */
	public function publishCd(){
		$img = unzip(C('ycdImgZipPath'),C('ycdImgPath'));
		$_POST['img'] = implode(',', $img);
		$model = D('Ycd');
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res != 0) {
				get_api_result(200,'添加成功');
			}else{
				get_api_result(300,'添加失败');			
			}
		}
	}

	/**
	 * 获取场地列表
	 *@param ycdid
	 *@param num
	 */
	public function getCdList(){
		$ycdid = I('ycdid',intval);
		$uid = I('uid',intval);
		$type = I('type',intval);
		$num = I('num',intval);
		$xian = I('xian');
		if ($ycdid==0) {
			$where = "type=$type and xian like '%$xian%'"; 
		}else{
			$where = "type=$type and xian like '%$xian%' and ycdid<$ycdid";
		}
		$model = M('ycd');
		$res = $model->where($where." and isdelete=0")->limit($num)->select();
		foreach ($res as $key => $value) {
			$img = explode(',', $res[$key]['img']);
			foreach ($img as $k => $v) {
				$img[$k] = httpImg($v);
			}
			$res[$key]['img'] = $img;
			$count = M('collect')->where("uid=$uid and type=2 and linkid=".$value['ycdid'])->count();
			$res[$key]['collect'] = $count==0?'no':'yes';
			$res[$key]['yhbcount'] = M('yhb')->where('ycdid='.$value['ycdid'])->count();
			$res[$key]['msgcount'] = M('ycdmsg')->where('ycdid='.$value['ycdid'])->count();
			$res[$key]['latlng'] = M('ycd')->where('ycdid='.$value['ycdid'])->field('lat,lng')->find();
		}
		get_api_result(200,$res);
	}

	/**
	 * 场地搜索
	 *@param name  场馆名称
	 */
	public function cdSearch(){
		$name = I('name');
		$res = M('ycd')->where("name like '%$name%'"." and isdelete=0")->select();
		if (!empty($res)) {
			foreach ($res as $key => $value) {
				$res[$key]['yhbcount'] = M('yhb')->where('ycdid='.$value['ycdid'])->count();
				$res[$key]['msgcount'] = M('ycdmsg')->where('ycdid='.$value['ycdid'])->count();
				$res[$key]['latlng'] = M('ycd')->where('ycdid='.$value['ycdid'])->field('lat,lng')->find();
				$img = explode(',', $res[$key]['img']);
				foreach ($img as $k => $v) {
					$img[$k] = httpImg($v);
				}
				$res[$key]['img'] = $img;
			}
			get_api_result(200,$res);
		}else{
			get_api_result(300,"没有此场馆~~");
		}
	}

	/**
	 * 场地留言
	 *@param uid
	 *@param ycdid
	 *@param content
	 */
	public function publishCdMsg(){
		$model = D('Ycdmsg');
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res != 0) {
				get_api_result(200,'成功');
			}else{
				get_api_result(300,'失败');		
			}
		}
	}

	/**
	 * 获取场地留言
	 *@param ycdmsgid (第一次传0)
	 *@param num
	 *@param ycdid
	 */
	public function getYcdmsgList(){
		$ycdmsgid = I('ycdmsgid',intval);
		$ycdid = I('ycdid',intval);
		$num = I('num',intval)==0?5:I('num',intval);
		$where = $ycdmsgid==0?"ycdid=$ycdid":"ycdid=$ycdid and ycdmsgid<$ycdmsgid";
		$res = M('ycdmsg')->where($where)->order('addtime DESC')->limit($num)->select();
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取在此场地的近期比赛
	 *@param ycdid
	 */
	public function getCdMatch(){
		$ycdid = I('ycdid',intval);
		$now = time();
		$res = M('teammatch')->where("isthrough=1 and matchdate>$now")->order('addtime ASC')->limit(2)->select();
		foreach ($res as $key => $value) {
			$ateamdata = M('team')->where("teamid=".$value['ateamid'])->field("name,img")->find();
			$res[$key]['ateamname'] = $ateamdata['name'];
			$res[$key]['ateamimg'] = httpImg($ateamdata['img']);
			$bteamdata = M('team')->where("teamid=".$value['bteamid'])->field("name,img")->find();
			$res[$key]['bteamname'] = $bteamdata['name'];
			$res[$key]['bteamimg'] = httpImg($bteamdata['img']);
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取收藏此场地的用户
	 *@param ycdid
	 */
	public function getCollectedUser(){
		$ycdid = I('ycdid',intval);
		$res = M('collect')->where("type=2 and linkid=$ycdid")->field('uid')->select();
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
		}
		get_api_result(200,$res);

	}

	/**
	 * 获取在此场地发布的活动
	 *@param ycdid
	 *@param yhbid
	 *@param num
	 */
	public function getYhbList(){
		$ycdid = I('ycdid',intval);
		$yhbid = I('yhbid',intval);
		$num = I('num',intval);
		$num = $num==0?5:$num;
		$where = $yhbid==0?"yhbid>0":"yhbid<$yhbid";
		$model = M('yhb');
		$res  = $model->where($where." and ycdid=$ycdid")->order('addtime DESC')->limit($num)->select();
		// echo $model->getDbError();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['usex'] = $userData['sex'];
			$res[$key]['borndate'] = $userData['borndate'];
			$xy = M('yhbxy')->where("uid=$uid and yhbid=".$value['yhbid'])->count();
			$res[$key]['isrespons'] = $xy==0?'no':'yes';
			$res[$key]['responscount'] = M('yhbxy')->where("yhbid=".$value['yhbid'])->count();
			$res[$key]['commentcount'] = M('yhbpl')->where("yhbid=".$value['yhbid'])->count();
		}
		get_api_result(200,$res);
	}




	/**
	 * 格式化时间
	 *第二个参数false时为时间戳 
	 */
	private function _getTimeFormat($the_time, $isString)
    {
        if ($isString)
        {
            $the_time = strtotime($the_time);
        }
        $show_time = date("m-d H:i", $the_time);
        $today = strtotime('today');
        $yesterday = $today - 24*3600;
        $durToday = time() - $today;
        $durYesterday = time() - $yesterday;
        $dur = time() - $the_time;
        if($dur < 0){
            return $show_time;
        }
        else
        {
            if($dur < 60)
            {
                return $dur.'秒前';
            }
            else
            {
                if($dur < 3600)
                {
                    return floor($dur/60).'分钟前';
                }
                else
                {
                    if($dur < 10800)
                    {
                        return floor($dur/3600).'小时前';
                    }
                    else
                    {
                        if($dur < $durToday)
                        {//3天内
                            return '今天 '.date('H:i', $the_time);
                        }
                        elseif ($dur < $durYesterday)
                        {
                            return '昨天 '.date('H:i', $the_time);
                        }
                        else
                        {
                            return $show_time;
                        }
                    }
                }
            }
        }
    }


}



 ?>