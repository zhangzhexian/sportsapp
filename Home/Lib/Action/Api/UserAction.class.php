<?php 
/**
* 用户模块
*/
class  UserAction extends CommonAction
{
	/**
	 * 用户注册
	 *@param tel
	 *@param pwd
	 *@param code
	 */
	public function register(){
		$tel = I('tel',intval);
		$pwd = I('pwd');
		$code = I('code',intval);
		if ($code==0) {
			get_api_result(305,'验证码不能为空');
		}else{
			$time = time()-1801;
			$res = M('validate')->where("tel='".$tel."' and addtime>$time")->order('addtime DESC')->getField('code');
			if ($res!=$code) {
				get_api_result(306,'验证码错误或过期');
			}
		}

		if (''==$tel||''==$pwd) {
			get_api_result(301,'用户名或密码不能为空');
		}
		if (!$this->checkTel($tel)){
			get_api_result(302,'手机号错误');
		}
		if(strlen($pwd)<6){
			get_api_result(303,'密码不能低于6位');
		}
		if (M('user')->where("tel=$tel")->count()!=0) {
			get_api_result(304,'该手机号已被注册');
		}
		$data['tel'] = $tel;
		$data['pwd'] = md5($pwd);
		$data['addtime'] = time();
		$model = M('user');
		$model->startTrans();
		$res = $model->add($data);
		$res1 = M('userdata')->add(array('uid'=>$res));
		if('0'!=$res && '0'!=$res1){
			$model->commit();
			get_api_result(200,"注册成功");
		}else{
			$model->rollback();
			get_api_result(300,"注册失败");
		}
	}

	/**
	 * 找回密码
	 *@param tel
	 *@param pwd
	 *@param code
	 */
	public function resetPwd(){
		$tel = I('tel',intval);
		$pwd = I('pwd');
		$code = I('code',intval);
		if ($code==0) {
			get_api_result(305,'验证码不能为空');
		}else{
			$time = time()-1801;
			$res = M('validate')->where("tel='".$tel."' and addtime>$time")->order('addtime DESC')->getField('code');
			if ($res!=$code) {
				get_api_result(306,'验证码错误或过期');
			}
		}

		if (''==$tel||''==$pwd) {
			get_api_result(301,'用户名或密码不能为空');
		}
		if (!$this->checkTel($tel)){
			get_api_result(302,'手机号错误');
		}
		if(strlen($pwd)<6){
			get_api_result(303,'密码不能低于6位');
		}
		if (M('user')->where("tel=$tel")->count()==0) {
			get_api_result(304,'该手机号未注册');
		}
		$data['pwd'] = md5($pwd);
		$model = M('user');
		$res = $model->where("tel=$tel")->save($data);
		if('0'!=$res){
			get_api_result(200,"修改成功");
		}else{
			get_api_result(300,"修改失败");
		}
	}

	/**
	 * 手机号码格式验证
	 *@param $tel
	 *@return bool
	 */
	protected function checkTel($tel){
		if(11!=strlen($tel))return false; 
		if(preg_match("/^13[0-9]{9}$|15[0-9]{9}$|18[0-9]{9}$/",$tel)){    
    		return true;
		}else{  
			return false;
		}
	}

	/**
	 * 用户登录
	 *@param tel
	 *@param pwd
	 *@param udid
	 */
	public function login(){
		$tel = I('tel',intval);
		$pwd = I('pwd');
		$udid = I('udid');
		if (''==$tel||''==$pwd||''==$udid) {
			get_api_result(301,'参数错误');
		}
		$data = M('user')->where("tel=$tel")->find();
		if ($data['pwd']==md5($pwd)) {
			if ($data['isdeny']==1) {
				get_api_result(302,'您的账号已被封停');
			}
			M('user')->where("tel=$tel")->save(array('udid'=>$udid));
			get_api_result(200,array('uid'=>$data['uid']));
		}else{
			get_api_result(300,'账号或密码错误');
		}

	}

	/**
	 * 设置个人信息
	 *@param uid
	 *@param nickname  昵称
	 *@param sex       1男  0女
	 *@param headimg   base64传
	 *@param idol      偶像
	 *@param signature 个性签名
	 *@param school		毕业院校
	 *@param company	工作单位
	 *@param position   场上位置
	 *@param borndate   出生日期
	 */
	public function setUserData(){
		$uid = I('uid',intval);
		if ($uid==0) {
			get_api_result(301,"参数错误");
		}
		$attriArr = M('userdata')->getDbFields();
		if (isset($_POST['headimg'])) $_POST['headimg'] = uploadBase64Img($_POST['headimg'],C('headImgPath'));
		foreach ($_POST as $key => $value) {
			if (!in_array($key, $attriArr)||$value=='') unset($_POST[$key]);
		}
		$data = $_POST;
		if (isset($_POST['company'])) {
			$data = $_POST;
			$data['school']='';
		}
		if(isset($_POST['school'])) {
			$data = $_POST;
			$data['company']='';
		}

		$model = M('userdata');
		$res = $model->where("uid=$uid")->save($data);
		if ($res!=0) {
			get_api_result(200,"设置成功");
		}else{
			zlog($model->getDbError().'>>>>'.$model->getLastSql());
			get_api_result(300,"设置失败");
		}
	}


	/**
	 * 获取用户信息
	  *@param uid
	  *@param selfuid
	 */
	public function getUserData()
	{
		$uid = I('uid',intval);
		$selfuid = I('selfuid',intval);
		$userData = M('userdata')->where("uid=$uid")->find();
		if (is_null($userData)) {
			$attriArr = M('userdata')->getDbFields();
			foreach ($attriArr as $value) {
				$userData[$value]='';
			}
		}
		$userData['headimg'] = httpImg($userData['headimg']);
		$userData['tag'] = $this->_formarUserTag($uid);
		$userData['tel'] = M('user')->where("uid=$uid")->getField('tel');
		$userData['fansCount'] = M('attention')->where("touid=$uid")->count();
		$userData['attentionCount'] = M('attention')->where("uid=$uid")->count();
		$userData['msgCount'] = M('teamuser')->where("uid=$uid and isthrough=0")->count();
		$userData['isattention'] = $this->checkAttention($selfuid,$uid);
		$userData['jifen'] = M("jf")->where("uid=$uid")->getField("sum(jf)");  //积分在此<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		$userData['jifen'] = is_null($userData['jifen'])?0:$userData['jifen'];  //积分在此<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		unset($userData['id']);
		get_api_result(200,$userData);
	}

	/**
	 * 关注用户
	 *@param uid
	 *@param touid
	 */
	public function attention(){
		$uid = I('uid',intval);
		$touid = checkUser(I('touid',intval));
		$count = M('attention')->where("uid=$uid and touid=$touid")->count();
		if ($count!=0) {
			get_api_result(301,"已关注该用户");
		}
		$data['uid'] = $uid;
		$data['touid'] = $touid;
		$res = M('attention')->add($data);
		if ($res!=0) {
			get_api_result(200,"关注成功");
		}else{
			get_api_result(300,"关注失败");
		}
	}

	/**
	 * 取消关注用户
	 *@param uid
	 *@param touid
	 */
	public function disAttention(){
		$uid = I('uid',intval);
		$touid = checkUser(intval(I('touid')));
		$count = M('attention')->where("uid=$uid and touid=$touid")->count();
		if ($count==0) {
			get_api_result(301,"您没有关注此用户");
		}
		$res = M('attention')->where("uid=$uid and touid=$touid")->delete();
		if ($res!=0) {
			get_api_result(200,"取消关注成功");
		}else{
			get_api_result(300,"取消关注失败");
		}
	}



	/**
	 * 查找用户
	 *@param tel
	 */
	public function searchUser(){
		$uid = I('uid');
		$tel = I('tel');
		$res = D('User')->relation('userdata')->where("tel=$tel")->field('uid')->find();
		if (is_null($res)) {
			get_api_result(300,"没有此用户");
		}
		$isattention = $this->checkAttention($uid,$res['uid']);
		$res['isattention'] = $isattention;
		$res['headimg'] = httpImg($res['headimg']);
		$uidArr = M('attention')->where("uid=".$res['uid'])->field('touid')->limit(6)->select();
		$friendData = array();
		foreach ($uidArr as $key => $value) {
			$model = M('userdata');
			$data = $model->where("uid=".$value['touid'])->field('uid,nickname,headimg,borndate')->find();
			if (is_null($data)) {
				$data['nickname'] = '';
				$data['headimg'] = '';
				$data['borndate'] = '';
				$data['uid'] = $value['touid'];
			}
			$data['headimg'] = httpImg($data['headimg']);
			$data['isattention'] = $this->checkAttention($uid,$value['touid']);
			$friendData[] = $data;
		}
		$res['tag'] = $this->_formarUserTag($uid);
		$res['attentionData'] = $friendData;
		get_api_result(200,$res);
	}


	/**
	 * 检查是否关注
	 *@param uid
	 *@param touid
	 */
	protected function checkAttention($uid , $touid){
		$model = M('attention');
		$isattention = $model->where("uid=$uid and touid=$touid")->count();
		$isattention = $isattention==0?'no':'yes';
		return $isattention;
	}

	/**
	 * 发送验证码
	 *@param tel
	 */
	public function sendVelidateCode(){
		$tel = I('tel',intval);
		$code = mt_rand(100000,999999);
		$res = $this->_sendVelidateCode($tel,$code);
		if ($res['error_code']==0) {
			$data['tel'] = $tel;
			$data['code'] = $code;
			$data['addtime'] = time();
			M('validate')->add($data);
			get_api_result(200,"验证码发送成功");
		}else{
			zlog('>>'.$res['reason']);
			get_api_result(300,"验证码发送失败");
		}
	}

	protected function _sendVelidateCode($tel,$code){
		$method = array();
		$param['tpl_id'] = 2922;
		$param['tpl_value'] = '%23code%23%3D'.$code;
		$param['mobile'] = $tel;
		$param['key'] = C('sms_key');
		$res = $this->getData($method,$param);
		return $res;
	}

	/**
	 * 获取关注的好友列表
	 */
	public function getFriendList(){
		$uid = I('uid',intval);
		$model = M('attention');
		$res = $model->join('left join userdata on attention.touid=userdata.uid')->where('attention.uid='.$uid)->field('attention.touid,userdata.nickname,userdata.headimg')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
				$res[$key]['headimg'] = httpImg($value['headimg']);
				$res[$key]['tel'] = M('user')->where('uid='.$value['touid'])->getField('tel');
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取关注的好友列表
	 */
	public function getFansList(){
		$uid = I('uid',intval);
		$model = M('attention');
		$res = $model->join('left join userdata on attention.uid=userdata.uid')->where('attention.touid='.$uid)->field('attention.uid,userdata.nickname,userdata.headimg')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
				$res[$key]['headimg'] = httpImg($value['headimg']);
				$res[$key]['tel'] = M('user')->where('uid='.$value['uid'])->getField('tel');
		}
		get_api_result(200,$res);
	}

	/**
	 * 三方登陆
	 *@openid
	 */
	public function otherLogin(){
		$qqopenid = I('qqopenid');
		$weixinopenid = I('weixinopenid');
		$weiboopenid = I('weiboopenid');
		$where = '';
		if ($qqopenid!='') {
			$where = "qqopenid='".$qqopenid."'";
		}
		if ($weixinopenid!='') {
			$where = "weixinopenid='".$weixinopenid."'";
		}
		if ($weiboopenid!='') {
			$where = "weiboopenid='".$weiboopenid."'";
		}
		if ($where=='') {
			get_api_result(301,'参数错误');
		}
		$uid = M('user')->where($where)->getField('uid');
		if (is_null($uid)) {
			get_api_result(300,"没有绑定账号");
		}else{
			get_api_result(200,array('uid'=>$uid));
		}
	}

	/**
	 * 三方登陆绑定
	 *@param uid
	 *@param openid
	 */
	public function otherLoginBind(){
		$uid = I('uid');
		$qqopenid = I('qqopenid');
		$weixinopenid = I('weixinopenid');
		$weiboopenid = I('weiboopenid');
		$data = array();
		if ($qqopenid!='') {
			$data['qqopenid'] = $qqopenid;
		}
		if ($weixinopenid!='') {
			$data['weixinopenid'] = $weixinopenid;
		}
		if ($weiboopenid!='') {
			$data['weiboopenid'] = $weiboopenid;
		}
		if (empty($data)||$uid=='') {
			get_api_result(301,'参数错误');
		}
		$res = M('user')->where("uid=$uid")->save($data);
		if ($data!=0) {
			get_api_result(200,'绑定成功');
		}else{
			get_api_result(300,'绑定失败');
		}
	}

	/**
	 * 添加用户标签
	 *@param uid
	 *@param tag
	 */
	public function addTag(){
		$uid = I('uid');
		$tag = I('tag');
		if ($uid==''||$tag=='') {
			get_api_result(301,'参数错误');
		}
		$count = M('usertag')->where("uid=$uid and tag='".$tag."'")->count();
		if ($count!=0) {
			get_api_result(302,'该标签已存在');
		}
		$res = M('usertag')->add(array('uid'=>$uid,'tag'=>$tag));
		if ($res!=0) {
			get_api_result(200,'添加成功');
		}else{
			get_api_result(300,'添加失败');
		}
	}

	/**
	 * 格式化用户标签
	 *@param uid
	 */
	protected function _formarUserTag($uid){
		$model =  M('usertag');
		$res = $model->where("uid=$uid")->select();
		$return = '';
		if (is_null($res)) {
			$return = "";
		}else{
			foreach ($res as $key => $value) {
				$return .= ",".$value['tag'];
			}
		}
		if ($return=='') {
			return $return;
		}
		return substr($return, 1);
	}


	/**
	 * 获取随机个性签名
	 */
	public function getSignature(){
		$res = M('signature')->order('rand()')->getField('signature');
		get_api_result(200,$res);
	}

	/**
	 * 获取同校或者同公司好友
	 *@param uid
	 */
	public function getRecFriend(){
		$uid = I('uid');
		$where='';
		$data = array();
		$res = M('userdata')->where('uid='.$uid)->field('school,company')->find();
		if ($res['company']!='') {
			$where = "company like '%".$res['company']."%'  and uid<>$uid";
			$flag = 'company';
		}
		if ($res['school']!='') {
			$where = "school like '%".$res['school']."%'  and uid<>$uid";
			$flag = 'school';
		}
		if ($where=='') {
			get_api_result(301,'个人资料未设置相关内容');
		}
		$res = M('userdata')->where($where)->order('rand()')->limit(6)->field('uid,nickname,headimg,borndate,sex')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			if ($value['uid'] == $uid) {
				continue;
			}
			$value['headimg'] = httpImg($value['headimg']);
			$value['isattention'] = $this->checkAttention($uid,$value['uid']);
			$data[] = $value;
		}
		$return['friendData'] = $data;
		$return['flag'] = $flag;
		get_api_result(200,$return);
	}

	/**
	 * 获取同偶像好友
	 *@param uid
	 */
	public function getSameIdolFriend(){
		$uid = I('uid');
		$idol = M('userdata')->where('uid='.$uid)->getField('idol');
		if ($idol=='') {
			get_api_result(301,'个人资料未设置相关内容');
		}
		$where = "idol like '%".$idol."%' and uid<>$uid";
		$res = M('userdata')->where($where)->order('rand()')->limit(6)->field('uid,nickname,headimg,borndate,sex')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			if ($value['uid'] == $uid) {
				continue;
			}
			$value['headimg'] = httpImg($value['headimg']);			
			$value['isattention'] = $this->checkAttention($uid,$value['uid']);
			$data[] = $value;
		}
		$return['friendData'] = $data;
		$return['flag'] = $idol;
		get_api_result(200,$return);
	}

	/**
	 * 获取用户标签
	 *@param uid
	 */
	public function getUserTag(){
		$uid = I('uid');
		$res = M('usertag')->where("uid=$uid")->order('zhan DESC')->select();
		$res = is_null($res)?array():$res;
		get_api_result(200,$res);
	}

	/**
	 * 用户标签踩或者赞
	 *@param usertagid
	 *@param type 1赞   0踩
	 */
	public function opUserTag(){
		$uid = I('uid');
		$usertagid = I('usertagid');
		$type = I('type');
		if ($type!=0 && $type!=1) {
			get_api_result(301,"type类型错误");
		}
		if($type==1) $res = M('usertag')->where("usertagid=$usertagid")->setInc('zhan');
		if($type==0) $res = M('usertag')->where("usertagid=$usertagid")->setInc('cai');
		if ($res!=0) {
			get_api_result(200,'成功');
		}else{
			get_api_result(300,'失败');
		}
	}

	/**
	 * 添加收藏
	 *@param uid
	 *@param type
	 *@param linkid
	 */
	public function addCollect(){
		$count = M('collect')->where("uid=".I('uid')." and type=".I('type')." and linkid=".I('linkid'))->count();
		if ($count!=0) {
			get_api_result(302,'收藏失败');
		}
		$model = D('Collect');
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res != 0) {
				get_api_result(200,'收藏成功');
			}else{
				get_api_result(300,'收藏失败');
			}
		}
	}

	/**
	 * 取消收藏
	 *@param uid
	 *@param type
	 *@param linkid
	 */
	public function cancelCollect(){
		$res = M('collect')->where("uid=".I('uid')." and type=".I('type')." and linkid=".I('linkid'))->delete();
		if ($res != 0) {
			get_api_result(200,'已取消');
		}else{
			get_api_result(300,'失败');
		}
		
	}

	/**
	 * 删除我的收藏
	 *@param uid
	 *@param collectid
	 */
	public function delCollect(){
		$uid = I('uid');
		$collectid = I('collectid');
		$tuid = M("collect")->where("collectid=$collectid")->getField('uid');
		if ($tuid != $uid) {
			get_api_result(301,"失败~");
		}
		$res = M('collect')->where("collectid=$collectid")->delete();
		if ($res != 0) {
			get_api_result(200,"成功~");
		}else{
			get_api_result(300,"失败~");
		}
	}

	/**
	 * 获取收藏列表
	 *@param uid
	 *@param type
	 *@param num
	 *@param collectid (第一次传0)
	 */
	public function getCollectList(){
		$uid = I('uid',intval);
		$type = I('type',intval);
		$num = I('num',intval);
		$collectid = I('collectid',intval);
		if ($collectid == 0) {
			$where = "uid=$uid and type=$type";
		}else{
			$where = "uid=$uid and type=$type and collectid<$collectid";
		}
		$res = M('collect')->where($where)->select();
		$array = array();
		foreach ($res as $key => $value) {
			$array[] = $value['linkid'];
		}
		switch ($type) {
			case 0:
				$this->_getYhbList($array);
				break;
			case 1:
				$this->_getMatchList($array);
				break;
			case 2:
				$this->_getYcdList($array);
				break;
		}
	}

	/**
	 * 获取联赛收藏列表
	 *@param array matchleagueid
	 */
	private function _getMatchList($array){
		$strList = implode(',', $array);
		if (!empty($strList)) {
			$model = M('matchleague');
			$res = $model->where("matchleagueid in ($strList)")->select();
			if (is_null($res)) {
				$res = array();
			}
			foreach ($res as $key => $value) {
				$res[$key]['collect'] = 'yes';
			}
		}else{
			$res = array();
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取活动收藏列表
	 *@param array yhbid
	 */
	private function _getYhbList($array){
		$strList = implode(',', $array);
		if (!empty($strList)) {
			$res  = M('yhb')->where("yhbid in ($strList)")->order('addtime DESC')->limit($num)->select();
			if (is_null($res)) {
				$res = array();
			}
			foreach ($res as $key => $value) {
				$userData = formatUser($value['uid']);
				$res[$key]['nickname'] = $userData['nickname'];
				$res[$key]['headimg'] = $userData['headimg'];
				$res[$key]['usex'] = $userData['sex'];
				$res[$key]['borndate'] = $userData['borndate'];
				$xy = M('yhbxy')->where("uid=$uid and yhbid=".$value['yhbid'])->count();
				$res[$key]['isrespons'] = $xy==0?'no':'yes';
				$res[$key]['responscount'] = M('yhbxy')->where("yhbid=".$value['yhbid'])->count();
				$res[$key]['commentcount'] = M('yhbpl')->where("yhbid=".$value['yhbid'])->count();
				$res[$key]['collect'] = 'yes';
			}
		}else{
			$res = array();
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取场馆收藏列表
	 *@param array ycdid
	 */
	private function _getYcdList($array){
		$strList = implode(',', $array);
		if (!empty($strList)) {
			$model = M('ycd');
			$res = $model->where("ycdid in ($strList)")->limit($num)->select();
			foreach ($res as $key => $value) {
				$img = explode(',', $res[$key]['img']);
				foreach ($img as $k => $v) {
					$img[$k] = httpImg($v);
				}
				$res[$key]['img'] = $img;
				$res[$key]['collect'] = 'yes';
			}
		}else{
			$res = array();
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取APP启动页
	 */
	public function getStartPage(){
		$img = M('startpage')->order('addtime DESC')->limit(1)->getField('img');
		$img = httpImg($img);
		get_api_result(200,$img);
	}


	  /**
     * 评论
     *@param uid
     *@param touid
     *@param content
     */
    public function userComment(){
        $model = D('Usermsg');
        $data = $model->create();
        if (!$model->create()) {
            get_api_result(301,$model->getError());
        }else{
            $res = $model->add();
            if (0 != $res) {
            	push(I('touid'),0);
                $userData = formatUser($data['uid']);
                $data['nickname'] = $userData['nickname'];
                $data['headimg'] = $userData['headimg'];
                $data['sex'] = $userData['sex'];
                $data['usermsgid'] = $res;
                get_api_result(200,$data);
            }else{
                get_api_result(300,"失败");
            }
        }
    }

    /**
     * 评论回复
     *@param uid
     *@param usermsgid
     *@param hfuid
     *@param content
     */
    public function replyComment(){
        $model = D('Usermsghf');
        $data = $model->create();
        if (!$model->create()) {
            get_api_result(301,$model->getError());
        }else{
            if (0 != $model->add()) {
            	push(I('hfuid'),1);
                $ud = formatUser($data['uid']);
                $hfud = formatUser($data['hfuid']);
                $data['nickname'] = $ud['nickname'];
                $data['sex'] = $ud['sex'];
                $data['hfnickname'] = $hfud['nickname'];
                $data['hfsex'] = $hfud['sex'];
                get_api_result(200,$data);
                
            }else{
                get_api_result(300,"失败");
            }
        }
    }

    /**
	 * 获取评论列表
	 *@param uid
	 *@param touid
	 *@param usermsgid
	 */
	public function getComment(){
		$uid = I('uid');
		$touid = I('touid');
		$usermsgid = I('usermsgid',intval);
		$num = I('num',intval);
		$num = $num==0?5:$num;
		$where = $usermsgid==0?"(touid=$touid or uid=$uid) and usermsgid>0":"(touid=$touid or uid=$uid) and usermsgid<$usermsgid";
		$count = M('usermsg')->where("touid=$touid")->count();
		$res = M('usermsg')->where($where)->order('addtime DESC')->limit($num)->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['sex'] = $userData['sex'];
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
			$hfpl = M('usermsghf')->where("usermsgid=".$value['usermsgid'])->order('usermsghfid ASC')->select();
			if (is_null($hfpl)) {
				$hfpl = array();
			}
			foreach ($hfpl as $k => $v) {
				$ud = formatUser($v['uid']);
				$hfud = formatUser($v['hfuid']);
				$hfpl[$k]['nickname'] = $ud['nickname'];
				$hfpl[$k]['sex'] = $ud['sex'];
				$hfpl[$k]['hfnickname'] = $hfud['nickname'];
				$hfpl[$k]['hfsex'] = $hfud['sex'];
			}
			if ($value['uid'] == $uid && empty($hfpl)) {
				unset($res[$key]);
				continue;
			}
			$res[$key]['hfdata'] = $hfpl;
		}
		$ret = array();
		foreach ($res as $key => $value) {
			$ret[] = $value;
		}
		get_api_result(200,$ret);
	}


	/**
	 * 获取积分列表
	 *@param  int uid
	 */
	public function getJfList(){
		$uid = I('uid');
		$model = D('Jf');
		$total = $model->getTotal($uid);
		$list = $model->getHis($uid);
		$data['total'] = $total;
		$data['jfhis'] = $list;
		get_api_result(200,$data);
	}
	/**
	 * 新闻分享时调用增加积分
	 */
	public function newsShareCallback(){
		$uid = I('uid');
		D('Jf')->addJf($uid,2);
	}

	public function push1(){
		var_dump(push(10,0));
	}

	/**
	 * 意见反馈
	 *@param uid
	 *@param content
	 */
	public function publishFaq(){
		$model = D('Faq');
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res != 0) {
				get_api_result(200,'成功');
			}else{
				get_api_result(300,'失败');			
			}
		}
	}

	/**
	 * 版本更新
	 *@param versionNo
	 */
	public function versionUpdate(){
		$versionNo = intval(I("versionNo"));
		$res = M('version')->where("versionNo>$versionNo")->order("versionNo DESC")->find();
		if (!is_null($res)) {
			$res['url'] = C('host').$res['path'];
			unset($res['path']);
			get_api_result(200,$res);
		}else{
			get_api_result(300,"已是最新版本~");
		}
	}

	/**
	 * 文件下载
	 */
	public function download(){
		$versionNo = intval(I('versionNo'));
		$path = M('version')->where("versionNo=$versionNo")->getField('path');
		header("Content-type:text/html;charset=utf-8");
        //用以解决中文不能显示出来的问题 
        // $file_name=iconv("utf-8","gb2312",$file_name); 
        // $file_sub_path='./Public/';
        $file_path="./".$file_name; 
        //首先要判断给定的文件存在与否 
        if(!file_exists( $file_path ) ){ 
           get_api_result(300,"已是最新版本~");
            die ; 
            } 
        $fp = fopen($file_path , "r"); 
        $file_size = filesize( $file_path );
        Header("Content-type: application/octet-stream"); 
        Header("Accept-Ranges: bytes"); 
        Header("Accept-Length:".$file_size); 
        Header("Content-Disposition: attachment; filename=".$file_name); 
        $buffer=1024; 
        $file_count=0; 
        //向浏览器返回数据 
        while(!feof($fp) && $file_count<$file_size){ 
            $file_con=fread($fp,$buffer); 
            $file_count+=$buffer; 
            echo $file_con;
        } 
        fclose($fp);
	}

	/**
	 * 格式化时间
	 *第二个参数false时为时间戳 
	 */
	private function _getTimeFormat($the_time, $isString)
    {
        if ($isString)
        {
            $the_time = strtotime($the_time);
        }
        $show_time = date("m-d H:i", $the_time);
        $today = strtotime('today');
        $yesterday = $today - 24*3600;
        $durToday = time() - $today;
        $durYesterday = time() - $yesterday;
        $dur = time() - $the_time;
        if($dur < 0){
            return $show_time;
        }
        else
        {
            if($dur < 60)
            {
                return $dur.'秒前';
            }
            else
            {
                if($dur < 3600)
                {
                    return floor($dur/60).'分钟前';
                }
                else
                {
                    if($dur < 10800)
                    {
                        return floor($dur/3600).'小时前';
                    }
                    else
                    {
                        if($dur < $durToday)
                        {//3天内
                            return '今天 '.date('H:i', $the_time);
                        }
                        elseif ($dur < $durYesterday)
                        {
                            return '昨天 '.date('H:i', $the_time);
                        }
                        else
                        {
                            return $show_time;
                        }
                    }
                }
            }
        }
    }
}





 ?>