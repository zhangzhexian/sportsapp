<?php 
/**
* 约伙伴id
*/
class YhbAction extends CommonAction
{
	
	/**
	 * 发布约伙伴信息
	 *@param uid
	 *@param location
	 *@param type
	 *@param sex
	 *@param date
	 *@param desc
	 *@param lat   （可选）
	 *@param lng   （可选）
	 */
	public function publishYhbMsg(){
		$model = D('Yhb');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			$data['yhbid'] = $res;
			$res!=0?get_api_result(200,$data):get_api_result(300,"发布失败");
		}
	}

	/**
	 * 响应约伙伴
	 *@param uid
	 *@param yhbid
	 */
	public function responseYhb(){
		$uid = I('uid');
		$yhbid = I('yhbid');
		if (0!=M('yhbxy')->where("uid=$uid and yhbid=$yhbid")->count()) {
			get_api_result(302,"您已响应了该活动~");
		}
		$data['uid'] = $uid;
		$data['yhbid'] = $yhbid;
		$res = M('yhbxy')->add($data);
		if ($res!=0) {
			get_api_result(200,"成功");
		}else{
			get_api_result(300,"失败");
		}
	}

	/**
	 * 约伙伴评论
	 *@param uid
	 *@param yhbid
	 *@param content
	 */
	public function yhbComment(){
		$model = D('Yhbpl');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if (0 != $res) {
				$userData = formatUser($data['uid']);
				$data['nickname'] = $userData['nickname'];
				$data['headimg'] = $userData['headimg'];
				$data['sex'] = $userData['sex'];
				$data['yhbplid'] = $res;
				get_api_result(200,$data);
			}else{
				get_api_result(300,"失败");
			}
		}
	}

	/**
	 * 约伙伴评论回复
	 *@param uid
	 *@param yhbid
	 *@param hfuid
	 *@param content
	 */
	public function replyComment(){
		$model = D('Hfpl');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			if (0 != $model->add()) {
				push(I('hfuid'),1);
				$data['hfplid'] = $res;
				$ud = formatUser($data['uid']);
				$hfud = formatUser($data['hfuid']);
				$data['nickname'] = $ud['nickname'];
				$data['sex'] = $ud['sex'];
				$data['hfnickname'] = $hfud['nickname'];
				$data['hfsex'] = $hfud['sex'];
				get_api_result(200,$data);
			}else{
				get_api_result(300,"失败");
			}
		}
	}

	/**
	 * 查看约伙伴详情
	 *@param uid
	 *@param yhbid
	 */
	public function getYhbInfo(){
		$uid = I('uid');
		$yhbid = I('yhbid');
		$info = M('yhb')->where("yhbid=$yhbid")->find();
		$userData = formatUser($info['uid']);
		$info['nickname'] = $userData['nickname'];
		$info['headimg'] = $userData['headimg'];
		$info['sex'] = $userData['sex'];
		$xy = M('yhbxy')->where("uid=$uid and yhbid=$yhbid")->count();
		$info['isrespons'] = $xy==0?'no':'yes';
		$info['issignal'] = 'no';
		$count = M('collect')->where("uid=$uid and type=0 and linkid=".$yhbid)->count();
		$info['collect'] = $count==0?'no':'yes';
		$xyArr = M('yhbxy')->where("yhbid=$yhbid")->field('uid,issignal')->select();
		if (is_null($xyArr)) {
			$xyArr= array();
		}
		$signaldata = array();
		foreach ($xyArr as $key => $value) {
			$userData = formatUser($value['uid']);
			$xyArr[$key]['nickname'] = $userData['nickname'];
			$xyArr[$key]['headimg'] = $userData['headimg'];
			$xyArr[$key]['sex'] = $userData['sex'];
			if ($value['issignal']==1) {
				$signaldata[] = $xyArr[$key];
			}
		}
		$info['responscount'] = count($xyArr);
		$info['responsdata'] = $xyArr;
		$info['signaldata'] = $signaldata;
		get_api_result(200,$info);
	}

	/**
	 * 获取约伙伴评论
	 *@param uid
	 *@param yhbid
	 *@param yhbplid
	 */
	public function getYhbComment(){
		$uid = I('uid');
		$yhbid = I('yhbid',intval);
		$yhbplid = I('yhbplid',intval);
		$num = I('num',intval);
		$num = $num==0?5:$num;
		$where = $yhbplid==0?"yhbid=$yhbid and yhbplid>0":"yhbid=$yhbid and yhbplid<$yhbplid";
		$count = M('yhbpl')->where("yhbid=$yhbid")->count();
		$res = M('yhbpl')->where($where)->order('addtime ASC')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['sex'] = $userData['sex'];
			$hfpl = M('hfpl')->where("yhbplid=".$value['yhbplid'])->order('addtime ASC')->select();
			if (is_null($hfpl)) {
				$hfpl = array();
			}
			foreach ($hfpl as $k => $v) {
				$ud = formatUser($v['uid']);
				$hfud = formatUser($v['hfuid']);
				$hfpl[$k]['nickname'] = $ud['nickname'];
				$hfpl[$k]['sex'] = $ud['sex'];
				$hfpl[$k]['hfnickname'] = $hfud['nickname'];
				$hfpl[$k]['hfsex'] = $hfud['sex'];
			}
			$res[$key]['hfdata'] = $hfpl;
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取约伙伴列表
	 *@param uid
	 *@param yhbid
	 *@param location
	 *@param num
	 *@param sex (可选参数)
	 *@param date  (可选参数)
	 *@param type  (可选参数)
	 */
	public function getYhbList(){
		$uid = I('uid');
		$sex = I('sex');
		$date = I('date');
		$type = I('type');
		$location = I('location');
		$yhbid = I('yhbid',intval);
		$num = I('num',intval);
		$num = $num==0?5:$num;
		$where = $yhbid==0?"yhbid>0":"yhbid<$yhbid";
		if ($sex !='') {
			$where .= " and sex=$sex";
		}
		if ($date !='') {
			$where .= " and date='".$date."'";
		}
		if ($type !='') {
			if ($type == 0) {
				$where .= " and type not in (1,2,3)";
			}else{
				$where .= " and type=$type";
			}
		}
		if ($location !='') {
			$where .= " and location='".$location."'";
		}
		$res  = M('yhb')->where($where." and ycdid=0")->order('addtime DESC')->limit($num)->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['usex'] = $userData['sex'];
			$res[$key]['borndate'] = $userData['borndate'];
			$xy = M('yhbxy')->where("uid=$uid and yhbid=".$value['yhbid'])->count();
			$res[$key]['isrespons'] = $xy==0?'no':'yes';
			$res[$key]['responscount'] = M('yhbxy')->where("yhbid=".$value['yhbid'])->count();
			$res[$key]['commentcount'] = M('yhbpl')->where("yhbid=".$value['yhbid'])->count();
		}
		get_api_result(200,$res);
	}

	/**
	 * 约伙伴签到
	 *@param yhbid
	 *@param uid
	 */
	public function yhbSignal(){
		$uid = I('uid');
		$yhbid = I('yhbid');
		$res = M('yhbxy')->where("yhbid=$yhbid and uid=$uid")->save(array('issignal'=>1));
		if ($res != 0) {
			get_api_result(200,"成功");
		}else{
			get_api_result(300,"失败");
		}
	}

	/**
	 * 约伙伴开关
	 *@param yhbid
	 *@param uid
	 */
	public function yhbSignalTurn(){
		$uid = I('uid');
		$yhbid = I('yhbid');
		$count = M('yhb')->where("yhbid=$yhbid and uid=$uid")->count();
		if ($count == 0) {
			get_api_result(403,"没有权限");
		}
		$issignal = M('yhb')->where("yhbid=$yhbid and uid=$uid")->getField('issignaloff');
		$data = array('issignaloff'=>($issignal+1)%2);
		$res = M('yhb')->where("yhbid=$yhbid and uid=$uid")->save($data);
		if ($res != 0) {
			get_api_result(200,$data);
		}else{
			get_api_result(300,"失败");
		}

	}


	/**
	 * 获取我发布的约伙伴信息
	 *@param uid
	 *@param yhbid
	 *@param num
	 *@param type
	 */
	public function getMyYhbList(){
		$uid = I('uid');
		$yhbid = I('yhbid');
		$num = I('num');
		$type = I('type');
		if ($type == 0) {
			$where = "uid=$uid";
		}else{
			$yhbArr = array();
			$list = M('yhbxy')->where("uid=$uid")->field('yhbid')->select();
			if (count($list)==1) {
				$where = 'yhbid='.$list[0]['yhbid'];
			}elseif (count($list)==0) {
				$where = '1=0';
			}else{
				foreach ($list as $key => $value) {
					$yhbArr[] = $value['yhbid'];
				}
				$where = 'yhbid in ('.implode(',', $yhbArr).')';
			}
		}
		$res  = M('yhb')->where($where)->order('addtime DESC')->limit($num)->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['usex'] = $userData['sex'];
			$res[$key]['borndate'] = $userData['borndate'];
			$xy = M('yhbxy')->where("uid=$uid and yhbid=".$value['yhbid'])->count();
			$res[$key]['isrespons'] = $xy==0?'no':'yes';
			$res[$key]['responscount'] = M('yhbxy')->where("yhbid=".$value['yhbid'])->count();
			$res[$key]['commentcount'] = M('yhbpl')->where("yhbid=".$value['yhbid'])->count();
		}
		get_api_result(200,$res);

	}
}




 ?>