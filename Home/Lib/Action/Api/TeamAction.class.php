<?php 
/**
* 球队
*/
class TeamAction extends CommonAction
{
	/**
	 * 创建球队
	 *@param uid
	 *@param name
	 *@param desc
	 *@param img base64
	 *@param type
	 *@param player example: [{"uid":"7","playernum":"8","role":"2"},{"uid":"8","playernum":"9","role":"1"}]
	 */
	public function createTeam(){
		$playerArr = I('player');
		$name = I('name');
		$playerArr = json_decode(htmlspecialchars_decode($playerArr),true);
		if (!$playerArr) {
			get_api_result(300,"参数错误");
		}
		if (preg_match('/屌/', $name)) {
			get_api_result(301,"该名称已存在~");
		}
		$count = M('team')->where("name like '".$name."'")->count();
		if ($count != 0) {
			get_api_result(302,"该名称已存在~");
		}

		$model = D('Team');
		$model->startTrans();
		$data = $model->create();
		if (!$model->create()){
    		get_api_result(301,$model->getError());
		}else{
			$data['img'] = uploadBase64Img($data['img'],C('teamHeaderPath'));
			if (isset($_POST['company'])) {
				$data['school']='';
			}
			if(isset($_POST['school'])) {
				$data['company']='';
			}
			$uid = I('uid');
			unset($data['uid']);
			$res1 = $model->add($data);
			$res2 = $this->addPlayer($playerArr,$uid,$res1);
			if ($res1!=0&&$res2!=false) {
				$model->commit();
				get_api_result(200,"创建成功!");
			}else{
				$model->rollback();
				get_api_result(300,$model->getError());
			}
		}
	}

	/**
	 * 修改球队
	 *@param teamid
	 *@param uid
	 *@param name
	 *@param desc
	 *@param img base64
	 *@param player example: [{"uid":"7","playernum":"8","role":"2"},{"uid":"8","playernum":"9","role":"1"}]
	 */
	public function editTeam(){
		$uid = I('uid');
		$playerArr = I('player');
		$name = I('name');
		$teamid = I('teamid');
		$playerArr = json_decode(htmlspecialchars_decode($playerArr),true);
		if (!$playerArr) {
			get_api_result(300,"参数错误");
		}
		if (preg_match('/屌/', $name)) {
			get_api_result(301,"该名称已存在~");
		}
		$count = M('team')->where("name like '".$name."' and teamid<>$teamid")->count();
		if ($count != 0) {
			get_api_result(302,"该名称已存在~");
		}
		$model = D('Team');
		$model->startTrans();
		$data = $model->create();
		if (!$model->create()){
    		get_api_result(301,$model->getError());
		}else{
			if (isset($_POST['img']) &&  $_POST['img'] != '') {
				$data['img'] = uploadBase64Img($data['img'],C('teamHeaderPath'));
			}else{
				unset($_POST['img']);
			}
			if (isset($_POST['company'])) {
				$data['school']='';
			}
			if(isset($_POST['school'])) {
				$data['company']='';
			}			
			unset($data['uid']);
			$res1 = $model->where("teamid=$teamid")->save($data);
			$res2 = $this->editPlayer($playerArr,$uid,$teamid);
			if ($res1!=0 || $res2!=false) {
				$model->commit();
				get_api_result(200,"修改成功!");
			}else{
				$model->rollback();
				get_api_result(300,"修改失败!");
			}
		}
	}	


	/**
	 * 修改球员
	 */
	public function editPlayer($playerArr,$uid,$teamid){
		$model = M('teamuser');
		foreach ($playerArr as $key => $value) {
			if($value['isthrough'] == "") unset($value['isthrough']);
			if(!checkUser($value['uid'],true)){
				$model->rollback();
				get_api_result(309,"用户".$value['uid']."不存在");
			}
			if (M('teamuser')->where("teamid=".$teamid." and uid=".$value['uid']." and isthrough<>3")->count()!=0) {				
				$res[] = $model->where("teamid=".$teamid." and uid=".$value['uid'])->save($value);
				continue;
			}
			$value['isthrough'] = $value['uid']==$uid?1:0;
			$value['teamid'] = $teamid; 
			$value['addtime'] = time();
			$res[] = $model->add($value);
		}
		foreach ($res as $value) {
			if ($value != 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 添加球员
	 */
	public function addPlayer($playerArr,$uid,$teamid){

		$model = M('teamuser');
		foreach ($playerArr as $key => $value) {
			if(!checkUser($value['uid'],true)){
				$model->rollback();
				get_api_result(309,"用户".$value['uid']."不存在");
			}
			if (M('teamuser')->where("teamid=".$teamid." and uid=".$value['uid'])->count()!=0) {
				continue;
			}
			$value['isthrough'] = $value['uid']==$uid?1:0;
			$value['teamid'] = $teamid; 
			$value['addtime'] = time();
			$res[] = $model->add($value);
		}
		if(!in_array('0', $res)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 添加球员
	 *@param player example: [{"uid":"7","teamid":"9","playernum":"8"},{"uid":"8","teamid":"9","playernum":"9"}]
	 */
	public function addTeamPlayer(){
		$playerArr = I('player');
		$playerArr = json_decode(htmlspecialchars_decode($playerArr),true);
		if (!$playerArr) {
			get_api_result(300,"参数错误");
		}
		$model = M('teamuser');
		$model->startTrans();
		foreach ($playerArr as $key => $value) {
			if(!checkUser($value['uid'],true)){
				$model->rollback();
				get_api_result(309,"用户".$value['uid']."不存在");
			}
			if (M('teamuser')->where("teamid=".$value['teamid']." and uid=".$value['uid'])->count()!=0) {
				continue;
			}
			$value['addtime'] = time();
			$res[] = $model->add($value);
		}
		if(!in_array('0', $res)){
			$model->commit();
			get_api_result(200,"邀请成功");
		}else{
			get_api_result(300,"邀请失败");
		}
	}

	/**
	 * 设置副队长
	 *@param uid
	 *@param teamid
	 */





	/**
	 * 创建球队相册
	 *@param teamid 
	 *@param catename 相册名称
	 *@param date 活动时间
	 */
	public function addTeamImgCate(){
		$teamid = I('teamid',intval);
		$catename = I('catename');
		$date = I('date');
		$data['teamid'] = $teamid;
		$data['name'] = $catename;
		$data['date'] = $date;
		$data['addtime'] = time();
		$res = M('teamimgcate')->add($data);
		if ($res!=0) {
			get_api_result(200,array('teamimgcateid'=>$res));
		}else{
			get_api_result(300,"创建失败");
		}
	}

	/**
	 * 获取球队相册列表
	 *@param teamid
	 */
	public function getTeamImgCateList(){
		$teamid = I('teamid',intval);
		$res = M('teamimgcate')->where("teamid=$teamid")->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$img = M('teamimg')->where('teamimgcateid='.$value['teamimgcateid'].' and iscover=1')->getField('img');
			if (is_null($img)) {
				$img = M('teamimg')->where('teamimgcateid='.$value['teamimgcateid'])->order('teamimgid DESC')->find();
				$img = is_null($img['img'])?'':httpImg($img['img']);
			}
			$res[$key]['cover'] = $img;
		}
		get_api_result(200,$res);
	}

	/**
	 * 相册图片上传
	 *@param teamimgcateid
	 *@param upload  图片zip压缩包
	 */
	public function uploadTeamImg(){
		$teamimgcateid = I('teamimgcateid',intval);
		$imgArr = unzip(C('teamImgZipPath'),C('teamImgPath'));
		$img1 = array();
		foreach ($imgArr as $key => $value) {
			$img['teamimgcateid'] = $teamimgcateid;
			$img['img'] = $value;
			$img1[] = $img;
		}
		$res = M('teamimg')->addAll($img1);
		if ($res!=0) {
			get_api_result(200,"上传成功");
		}else{
			get_api_result(300,"上传失败");
		}
	}

	/**
	 * 获取相册图片
	 *@param teamimgcateid
	 */
	public function getTeamCateImg(){
		$teamimgcateid = I('teamimgcateid',intval);
		$res = M('teamimg')->where("teamimgcateid=$teamimgcateid")->field('img')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$res[$key]['img'] = httpImg($value['img']);
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取球队及球队成员信息
	 *@param int teamid
	 */

	public function getTeamInfo(){
		$teamid = I('teamid',intval);
		$teamData = M('team')->where("teamid=$teamid")->find();
		$teamData['img'] = httpImg($teamData['img']);
		$teamUserData = M('teamuser')->join('left join userdata on teamuser.uid=userdata.uid')->field('userdata.uid,userdata.nickname,userdata.headimg,teamuser.role,teamuser.playernum,teamuser.isthrough')->where("teamuser.teamid=$teamid and teamuser.isthrough<>3")->select();
		if (is_null($teamUserData)) {
			$teamUserData = array();
		}
		foreach ($teamUserData as $key => $value) {
			$teamUserData[$key]['headimg'] = httpImg($value['headimg']);
			$teamUserData[$key]['tel'] = M('user')->where("uid=".$value['uid'])->getField('tel');
		}
		$return['teamData'] = $teamData;
		$return['teamUserData'] = $teamUserData;
		get_api_result(200,$return);
	}

	/**
	 * 发布球队留言
	 *@param teamid
	 *@param uid
	 *@param msg
	 */
	public function publishTeamMsg(){
		$teamid = I('teamid',intval);
		$uid = I('uid',intval);
		$msg = I('msg');
		if ($teamid==0 || $msg=='') {
			get_api_result(301,"参数错误");
		}
		$data['teamid'] = $teamid;
		$data['uid'] = $uid;
		$data['msg'] = $msg;
		$data['addtime'] = time();
		$res = M('teammsg')->add($data);
		if ($res != 0) {
			$value = formatUser($data['uid']);
			$data['headimg'] = $value['headimg'];
			$data['nickname'] = $value['nickname'];
			$data['addtime'] = $this->_getTimeFormat($data['addtime'],false);
			$data['praiseCount'] = 0;
			$count = M('teammsgpraise')->where("teammsgid=".$res." and uid=$uid")->count();
			$data['isPraise'] = 'no';
			$data['teammsgid'] = $res;
			get_api_result(200,$data);
		}else{
			get_api_result(300,"留言失败");
		}
	}

	/**
	 * 获取球队留言
	 *@param teamid
	 */
	public function getTeamMsgList(){
		$teamid = I('teamid',intval);
		$teammsgid = I('teammsgid',intval);
		$num = I('num',intval);
		$uid = I('uid',intval);
		$where = $teammsgid==0?"teammsg.teamid=$teamid":"teammsg.teamid=$teamid and teammsg.teammsgid<$teammsgid";
		$num = $num==0?5:$num;
		$model = M('teammsg');
		$totalCount = M('teammsg')->where("teamid=$teamid")->count();
		$res = $model->join('left join userdata on teammsg.uid=userdata.uid')->where($where)->field('teammsg.uid,userdata.nickname,userdata.headimg,teammsg.msg,teammsg.addtime,teammsg.teammsgid')->limit($num)->order('addtime DESC')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$res[$key]['headimg'] = httpImg($value['headimg']);
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
			$res[$key]['praiseCount'] = M('teammsgpraise')->where("teammsgid=".$value['teammsgid'])->count();
			$count = M('teammsgpraise')->where("teammsgid=".$value['teammsgid']." and uid=$uid")->count();
			$res[$key]['isPraise'] = $count==0?'no':'yes';
		}
		$data['totalCount'] = $totalCount;
		$data['msg'] = $res;
		get_api_result(200,$data);
	}

	/**
	 * 球队留言点赞
	 *@param id
	 *@param uid
	 */
	public function teamMsgPraise(){
		$teammsgid = I('teammsgid',intval);
		$uid = I('uid',intval);
		$count = M('teammsgpraise')->where("teammsgid=$teammsgid and uid=$uid")->count();
		if ($count!=0) {
			get_api_result(301,"您已经点过赞了");
		}
		$data['uid'] = $uid;
		$data['teammsgid'] = $teammsgid;
		$res = M('teammsgpraise')->add($data);
		if ($res!=0) {
			get_api_result(200,"点赞成功");
		}else{
			get_api_result(300,"点赞失败");
		}

	}

	/**
	 * 关注球队
	 *@param uid
	 *@param teamid
	 */
	public function teamAttention(){
		$uid = I('uid',intval);
		$teamid = I('teamid',intval);
		$count = M('teamattention')->where("uid=$uid and teamid=$teamid")->count();
		if ($count!=0) {
			get_api_result(301,"你已经关注过该球队了");
		}
		$data['uid'] = $uid;
		$data['teamid'] = $teamid;
		$res = M('teamattention')->add($data);
		if ($res!=0) {
			get_api_result(200,"关注成功");
		}else{
			get_api_result(300,"关注失败");
		}
	}


	/**
	 * 取消关注球队
	 *@param uid
	 *@param teamid
	 */
	public function disTeamAttention(){
		$uid = I('uid',intval);
		$teamid = I('teamid',intval);
		$count = M('teamattention')->where("uid=$uid and teamid=$teamid")->count();
		if ($count==0) {
			get_api_result(301,"你没有关注过该球队");
		}
		$res = M('teamattention')->where("uid=$uid and teamid=$teamid")->delete();
		if ($res!=0) {
			get_api_result(200,"取消关注成功");
		}else{
			get_api_result(300,"取消关注失败");
		}
	}

	/**
	 * 获取我关注的球队
	 *@param uid
	 */
	public function getAttentionTeam(){
		$uid = I('uid',intval);
		$res = M('teamattention')->join('left join team on teamattention.teamid=team.teamid')->where("teamattention.uid=$uid")->field('team.teamid,team.name,team.img,team.desc')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$res[$key]['img'] = httpImg($value['img']);
			$playerData = M('teamuser')->join('left join userdata on teamuser.uid=userdata.uid')->where("teamid=".$value['teamid']." and role in (1,2)")->field('userdata.uid,userdata.nickname,userdata.headimg,teamuser.role')->select();
			foreach ($playerData as $k => $v) {
				$playerData[$k]['headimg'] = httpImg($v['headimg']);
				$playerData[$k]['tel'] = M('user')->where("uid=".$v['uid'])->getField('tel');
			}
			$res[$key]['playerData'] = $playerData;
		}
		get_api_result(200,$res);
	}

	/**
	 * 获取我加入的球队
	 *@param uid
	 */
	public function getJoinTeam(){
		$uid = I('uid',intval);
		$res = M('teamuser')->join('left join team on teamuser.teamid=team.teamid')->where("teamuser.uid=$uid and isthrough=1")->field('team.teamid,team.name,team.img,team.desc')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$count = M('teamattention')->where("uid=$uid and teamid=".$value['teamid'])->count();
			$count = $count==0?'0':'1';
			$res[$key]['img'] = httpImg($value['img']);
			$playerData = M('teamuser')->join('left join userdata on teamuser.uid=userdata.uid')->where("teamid=".$value['teamid']." and role in (1,2) and isthrough=1")->field('userdata.uid,userdata.nickname,userdata.headimg,teamuser.role')->select();
			foreach ($playerData as $k => $v) {
				$playerData[$k]['headimg'] = httpImg($v['headimg']);
				$playerData[$k]['tel'] = M('user')->where("uid=".$v['uid'])->getField('tel');
			}
			$res[$key]['isattention'] = $count;
			$res[$key]['playerData'] = $playerData;
		}
		get_api_result(200,$res);
	}

	/**
	 * 搜索球队
	 *@param name 球队名称
	 *@param uid
	 */
	public function searchTeam(){
		$teamName = I('name');
		$uid = I('uid');
		$res = M('team')->where("name like '%".$teamName."%'")->field('teamid,name,img,desc')->select();
		if (is_null($res)) {
			get_api_result(300,'没有该球队');
		}else{
			foreach ($res as $key => $value) {
				$count = M('teamattention')->where('teamid='.$value['teamid'].' and uid='.$uid)->count();
				$res[$key]['isattention'] = $count==0?'no':'yes';
				$res[$key]['img'] = httpImg($value['img']);
			}
			get_api_result(200,$res);
		}
	}

	/**
	 * 获取同校或者同公司球队
	 *@param uid
	 */
	public function getSameTeam(){
		$uid = I('uid');
		$where='';
		$data = array();
		$res = M('userdata')->where('uid='.$uid)->field('school,company')->find();
		if ($res['company']!='') {
			$where = "company like '%".$res['company']."%'";
			$flag = 'company';
		}
		if ($res['school']!='') {
			$where = "school like '%".$res['school']."%'";
			$flag = 'school';
		}
		if ($where=='') {
			get_api_result(301,'个人资料未设置相关内容');
		}
		$res = M('team')->where($where)->order('rand()')->limit(6)->field('teamid,name,img')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$idArr = M('teamuser')->where("uid=$uid and isthrough=1")->field('teamid')->select();
			$af = 0;
			foreach ($idArr as $k => $v) {
				if ($v['teamid'] == $value['teamid']) {
					$af = 1;
				}
			}
			if ($af==1) {
				continue;
			}
			$value['img'] = httpImg($value['img']);
			$value['isattention'] = $this->_checkTeamAttention($uid,$value['teamid']);
			$data[] = $value;
		}
		$return['teamData'] = $data;
		$return['flag'] = $flag;
		get_api_result(200,$return);
	}

	/**
	 * 随便看看推荐球队
	 *@param uid
	 */
	public function getRecTeam(){
		$uid = I('uid');
		$res = M('team')->order('rand()')->limit(4)->field('teamid,name,img')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$idArr = M('teamuser')->where("uid=$uid and isthrough=1")->field('teamid')->select();
			$af = 0;
			foreach ($idArr as $k => $v) {
				if ($v['teamid'] == $value['teamid']) {
					$af = 1;
				}
			}
			if ($af==1) {
				continue;
			}
			$value['img'] = httpImg($value['img']);
			$value['isattention'] = $this->_checkTeamAttention($uid,$value['teamid']);
			$data[] = $value;
		}
		get_api_result(200,$data);
	}

	/**
	 * 加入球队处理
	 *@param teamid
	 *@param uid
	 *@param type  (1通过  2拒绝)
	 */
	public function joinTeamHandle(){
		$uid = I('uid');
		$teamid = I('teamid');
		$type = I('type')%3;
		$res = M('teamuser')->where("uid=$uid and teamid=$teamid")->save(array('isthrough'=>$type));
		if ($res != 0) {
			get_api_result(200,'成功~');
		}else{
			get_api_result(300,'失败~');
		}
	}


	/**
	 * 获取加入球队邀请列表
	 *@param uid
	 */
	public function getJoinTeamList(){
		$uid = I('uid');
		$res = M('teamuser')->join('left join userdata ud on teamuser.uid=ud.uid left join team t on teamuser.teamid=t.teamid')->where("teamuser.uid=$uid and teamuser.role<>2")->field('ud.nickname,ud.uid,ud.sex,t.name as teamname,t.teamid,teamuser.isthrough')->order('teamuser.addtime DESC')->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$uid = M('teamuser')->where("teamid=".$value['teamid']." and role=2")->getField('uid');
			$ud = formatUser($uid);
			// echo $uid;continue;
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['sex'] = $ud['sex'];
			$res[$key]['uid'] = $uid;
		}
		get_api_result(200,$res);
	}

	/**
	 * 向其他球队发起挑战
	 *@param ateamid
	 *@param bteamid
	 *@param ycdid	
	 *@param matchdate
	 *@param othernote（可选）
	 */
	public function teamChallenge(){
		$ateamid = I('ateamid',intval);
		$bteamid = I('bteamid',intval);
		if ($ateamid == $bteamid) {
			get_api_result(300,"不能挑战自己~");
		}
		$matchdate = I('matchdate');
		$ycdid = I('ycdid',intval);
		$othernote = I('othernote',intval);
		$addtime = time();
		$model = new Model();
		$res = $model->query("insert into teammatch (ateamid,bteamid,addtime,matchdate,ycdid,othernote) values ($ateamid,$bteamid,$addtime,'$matchdate',$ycdid,'$othernote')");
		if ($res !=0 ) {
			get_api_result(200,'发起挑战成功');
		}else{
			get_api_result(300,$model->getError());
		}
	}

	/**
	 * 获取其他球队挑战列表
	 *@param teamid
	 */
	public function getChallengeList(){ 
		$teamid = I('teamid',intval);
		$model = new Model();
		$res = M('teammatch tm')->join("team at on at.teamid=tm.ateamid")->where("tm.bteamid=$teamid")->field('at.teamid,at.name,at.img,tm.addtime,tm.teammatchid,tm.matchdate,tm.ycdid,tm.othernote,tm.isthrough,tm.ycdid,at.desc')->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$res[$key]['img'] = httpImg($value['img']);
			$res[$key]['playerdata'] = formatTeamLeader($value['teamid']);
			$res[$key]['cdname'] = M('ycd')->where("ycdid=".$value['ycdid'])->getField('name');
		}
		get_api_result(200,$res);
	}


	/**
	 * 获取球队发出的挑战列表
	 *@param teamid
	 */
	public function getSendChallengeList(){
		$teamid = I('teamid',intval);
		$model = new Model();
		$res = M('teammatch tm')->join("team at on at.teamid=tm.bteamid")->where("tm.ateamid=$teamid")->field('at.teamid,at.name,at.img,tm.addtime,tm.teammatchid,tm.matchdate,tm.ycdid,tm.othernote,tm.isthrough,tm.ycdid,at.desc')->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$res[$key]['img'] = httpImg($value['img']);
			$res[$key]['playerdata'] = formatTeamLeader($value['teamid']);
			$res[$key]['cdname'] = M('ycd')->where("ycdid=".$value['ycdid'])->getField('name');
		}
		get_api_result(200,$res);
	}


	/**
	 * 球队挑战处理
	 *@param teammatchid 
	 *@param type 1接受挑战  2拒绝挑战
	 */
	public function teamChallengeHandle(){
		$teammatchid = I('teammatchid',intval);
		$type = I('type',intval)%3;
		$model = new Model();
		$res = M('teammatch')->where("teammatchid=$teammatchid")->save(array('isthrough'=>$type));
		if ($res !=0 ) {
			get_api_result(200,'成功');
		}else{
			get_api_result(300,"失败");
		}
	}

	/**
	 * 发布战术板消息
	 *@param uid
	 *@param teamid
	 *@param content
	 */
	public function publishZsbMsg(){
		$uid = I('uid');
		$teamid = I('teamid');
		$role = M('teamuser')->where("uid=$uid and teamid=$teamid")->getField("role");
		if ( $role==0 ) {
			get_api_result(304,"没有权限发布消息");
		}
		$model = D('Zsb');
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res !=0 ) {
				get_api_result(200,array('zsbid'=>$res));
			}else{
				get_api_result(300,"失败");
			}
		}
	}

	/**
	 * 获取战术板消息列表
	 *@param teamid
	 */
	public function getZsbMsgList(){
		$teamid = I('teamid');
		$res = M('zsb')->where("teamid=$teamid")->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$res[$key]['playernum'] = M('teamuser')->where("teamid=".$value['teamid']." and uid=".$value['uid'])->getField('playernum');
			$res[$key]['role'] = M('teamuser')->where("teamid=".$value['teamid']." and uid=".$value['uid'])->getField('role');
			$res[$key]['commentnum'] = M('zsbpl')->where("zsbid=".$value['zsbid'])->count();
		}
		get_api_result(200,$res);
	}

	/**
	 * 发布战术板消息评论
	 *@param uid
	 *@param zsbid
	 *@param plcontent
	 *@param teamid
	 */
	public function publishZsbMsgPl(){
		$uid = I('uid');
		$teamid = I('teamid');
		$count = M('teamuser')->where("uid=$uid and teamid=$teamid and isthrough=1")->count();
		if ($count == 0) {
			get_api_result(304,"没有权限发布消息");
		}
		$model = D('Zsbpl');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res !=0 ) {
				$userData = formatUser($data['uid']);
				$data['nickname'] = $userData['nickname'];
				$data['headimg'] = $userData['headimg'];
				$data['sex'] = $userData['sex'];
				$data['zsbplid'] = $res;
				get_api_result(200,$data);
			}else{
				get_api_result(300,"失败");
			}
		}
	}

	/**
	 * 战术板评论回复
	 *@param uid
	 *@param hfuid
	 *@param zsbplid
	 *@param content
	 *@param teamid
	 */
	public function publishZsbMsgPlHf(){
		$uid = I('uid');
		$teamid = I('teamid');
		$count = M('teamuser')->where("uid=$uid and teamid=$teamid and isthrough=1")->count();
		if ($count == 0) {
			get_api_result(304,"没有权限发布消息");
		}
		$model = D('Zsbplhf');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res !=0 ) {
				$data['zsbplhfid'] = $res;
				$ud = formatUser($data['uid']);
				$hfud = formatUser($data['hfuid']);
				$data['nickname'] = $ud['nickname'];
				$data['sex'] = $ud['sex'];
				$data['hfnickname'] = $hfud['nickname'];
				$data['hfsex'] = $hfud['sex'];
				get_api_result(200,$data);
			}else{
				get_api_result(300,"失败");
			}
		}
	}


	/**
	 * 获取战术板评论
	 *@param zsbid
	 */
	public function getZsbPlList(){
		$zsbid = I('zsbid');
		$zsbplid = I('zsbplid',intval);
		$num = I('num',intval)==0?5:I('num',intval);
		$teamid = I('teamid');
		if ($zsbplid == 0) {
			$where = "zsbid=$zsbid";
		}else{
			$where = "zsbid=$zsbid and zsbplid<$zsbplid";
		}
		$res = M('zsbpl')->where($where)->order('zsbplid DESC')->select();
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$res[$key]['playernum'] = M('teamuser')->where("teamid=".$teamid." and uid=".$value['uid'])->getField('playernum');
			$res[$key]['role'] = M('teamuser')->where("teamid=".$teamid." and uid=".$value['uid'])->getField('role');
			$plhfData = M('zsbplhf')->where('zsbplid='.$value['zsbplid'])->select();
			foreach ($plhfData as $k => $v) {
				$ud1 = formatUser($v['uid']);
				$plhfData[$k]['nickname'] = $ud1['nickname'];
				$plhfData[$k]['sex'] = $ud1['sex'];
				$plhfData[$k]['playernum'] = M('teamuser')->where("teamid=".$teamid." and uid=".$v['uid'])->getField('playernum');
				$ud1 = formatUser($v['hfuid']);
				$plhfData[$k]['hfnickname'] = $ud1['nickname'];
				$plhfData[$k]['hfsex'] = $ud1['sex'];
				$plhfData[$k]['hfplayernum'] = M('teamuser')->where("teamid=".$teamid." and uid=".$v['hfuid'])->getField('playernum');
			}
			$res[$key]['hfdata'] = $plhfData;
		}
		get_api_result(200,$res);
	}




	/**
	 *检测是否关注球队
	 *@param uid
	 *@param teamid
	 */
	public function _checkTeamAttention($uid,$teamid){
		$count = M('teamattention')->where("uid=$uid and teamid=$teamid")->count();
		return $count==0?'no':'yes';
	}


	/**
	 * 格式化时间
	 *第二个参数false时为时间戳 
	 */
	private function _getTimeFormat($the_time, $isString)
    {
        if ($isString)
        {
            $the_time = strtotime($the_time);
        }
        $show_time = date("m-d H:i", $the_time);
        $today = strtotime('today');
        $yesterday = $today - 24*3600;
        $durToday = time() - $today;
        $durYesterday = time() - $yesterday;
        $dur = time() - $the_time;
        if($dur < 0){
            return $show_time;
        }
        else
        {
            if($dur < 60)
            {
                return $dur.'秒前';
            }
            else
            {
                if($dur < 3600)
                {
                    return floor($dur/60).'分钟前';
                }
                else
                {
                    if($dur < 10800)
                    {
                        return floor($dur/3600).'小时前';
                    }
                    else
                    {
                        if($dur < $durToday)
                        {//3天内
                            return '今天 '.date('H:i', $the_time);
                        }
                        elseif ($dur < $durYesterday)
                        {
                            return '昨天 '.date('H:i', $the_time);
                        }
                        else
                        {
                            return $show_time;
                        }
                    }
                }
            }
        }
    }
}




 ?>