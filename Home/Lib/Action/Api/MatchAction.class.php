<?php 
/**
* 
*/
class MatchAction extends CommonAction
{
	
	/**
	 * 获取联赛列表
	 */
	public function getMatchLeagueList(){
		$location = I('location');
		$uid = I('uid');
		$type = I('type');
		if ($location==''||$type=='') {
			get_api_result(301,'参数错误');
		}
		$res = M('matchleague')->where("location like '".$location."' and type=$type")->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$count = M('collect')->where("uid=$uid and type=1 and linkid=".$value['matchleagueid'])->count();
			$res[$key]['collect'] = $count==0?'no':'yes';
		}
		get_api_result(200,$res);
	}


	/**
	 * 联赛搜索
	 *@param name
	 */
	public function matchleagueSearch(){
		$name = I('name');
		$uid = I('uid');
		$res = M('matchleague')->where("name like '%".$name."%'")->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$count = M('collect')->where("uid=$uid and type=1 and linkid=".$value['matchleagueid'])->count();
			$res[$key]['collect'] = $count==0?'no':'yes';
		}
		get_api_result(200,$res);

	}

	/**
	 * 获取联赛比赛列表
	 *@param matchleagueid
	 */
	public function getMatchList(){
		$matchleagueid = I('matchleagueid',intval);
		$model = M('match');
		$res = $model->where("matchleagueid=$matchleagueid")->field('matchid,date,ateamid,bteamid,ateamscore,bteamscore,mvpsupportoff,matchlocation')->select();
		// echo $model->getDbError();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ateamData = M('team')->where("teamid=".$value['ateamid'])->field('name,img')->find();
			$bteamData = M('team')->where("teamid=".$value['bteamid'])->field('name,img')->find();
			$res[$key]['ateamname'] = $ateamData['name'];
			$res[$key]['ateamimg'] = httpImg($ateamData['img']);
			$res[$key]['bteamname'] = $bteamData['name'];
			$res[$key]['bteamimg'] = httpImg($bteamData['img']);
		}
		get_api_result(200,$res);
	}

	 /**
	  * 获取比赛详情
	  *@param matchid
	  */
	 public function getMatchInfo(){
	 	$matchid = I('matchid',intval);
	 	$topData = M('match')->where("matchid=$matchid")->field('matchid,date,ateamid,bteamid,ateamscore,bteamscore,ateamsupport,bteamsupport,mvpsupportoff')->find();
	 	$ateamData = M('team')->where("teamid=".$topData['ateamid'])->field('name,img')->find();
		$bteamData = M('team')->where("teamid=".$topData['bteamid'])->field('name,img')->find();
		$topData['ateamname'] = $ateamData['name'];
		$topData['ateamimg'] = httpImg($ateamData['img']);
		$topData['bteamname'] = $bteamData['name'];
		$topData['bteamimg'] = httpImg($bteamData['img']);
	 	$bottomAteam = M('matchdata')->where("matchid=$matchid and teamid=".$topData['ateamid'])->order('goal DESC')->field('uid,goal,assists')->limit(3)->select();
	 	$bottomAteam = is_null($bottomAteam)?array():$bottomAteam;
	 	foreach ($bottomAteam as $key => $value) {
	 		$userData = formatUser($value['uid']);
	 		$bottomAteam[$key]['nickname'] = $userData['nickname'];
	 		$bottomAteam[$key]['headimg'] = $userData['headimg'];
	 		$bottomAteam[$key]['postion'] = $userData['postion'];
	 	}
	 	$bottomBteam = M('matchdata')->where("matchid=$matchid and teamid=".$topData['bteamid'])->order('goal DESC')->field('uid,goal,assists')->limit(3)->select();
	 	$bottomBteam = is_null($bottomBteam)?array():$bottomBteam;
	 	foreach ($bottomBteam as $key => $value) {
	 		$userData = formatUser($value['uid']);
	 		$bottomBteam[$key]['nickname'] = $userData['nickname'];
	 		$bottomBteam[$key]['headimg'] = $userData['headimg'];
	 		$bottomBteam[$key]['postion'] = $userData['postion'];
	 	}
	 	$bottomData['ateam'] = $bottomAteam;
	 	$bottomData['bteam'] = $bottomBteam;
	 	$mvpRes = M('matchmvp')->where('matchid='.$matchid)->field('playeruid,count(*) as count')->order('count DESC')->find();
	 	if ($mvpRes['playeruid']!=0) {
	 		$mvpData['uid'] = $mvpRes['playeruid'];
		 	$mvpUserInfo = formatUser($mvpRes['playeruid']);
		 	$mvpData['nickname'] = $mvpUserInfo['nickname'];
		 	$mvpData['headimg'] = $mvpUserInfo['headimg'];
		 	$mvpData['postion'] = $mvpUserInfo['postion'];
		 	$res['mvpData'] = $mvpData;
	 	}
	 	$res['topData'] = $topData;
	 	$res['bottomData'] = $bottomData;
	 	get_api_result(200,$res);
	 }	

	 /**
	  * 支持球队
	  *@param team  'ateam' OR 'bteam'
	  *@param matchid
	  */
	 public function matchSupport(){
	 	$team = I('team');
	 	$matchid = I('matchid',intval);
	 	if ($team!='ateam'&&$team!='bteam') {
	 		get_api_result(301,'参数team错误');
	 	}
	 	$res = M('match')->where('matchid='.$matchid)->setInc($team.'support');
	 	if ($res!=0) {
	 		get_api_result(200,'成功');
	 	}else{
	 		get_api_result(300,'失败');
	 	}
	 }

	 /**
	  * mvp投票球员列表
	  *@param matchid
	  */
	 public function getMatchMvpList(){
	 	$matchid = I('matchid',intval);
	 	$uid = I('uid',intval);
	 	$res = M('match')->where("matchid=$matchid")->field('ateamid,bteamid')->find();
	 	$aTeamData = M('team')->where('teamid='.$res['ateamid'])->field('name,img')->find();
	 	$bTeamData = M('team')->where('teamid='.$res['bteamid'])->field('name,img')->find();
	 	$aTeamData['img'] = httpImg($aTeamData['img']);
	 	$bTeamData['img'] = httpImg($bTeamData['img']);
	 	$aTeamPlayer = M('teamuser')->where("isthrough=1 and teamid=".$res['ateamid'])->field('uid')->select();
	 	if (is_null($aTeamPlayer)) {
	 		$aTeamPlayer = array();
	 	}
	 	foreach ($aTeamPlayer as $key => $value){
	 		$userRes = formatUser($value['uid']);
	 		$aTeamPlayer[$key]['nickname'] = $userRes['nickname'];
	 		$aTeamPlayer[$key]['headimg'] = $userRes['headimg'];
	 		$aTeamPlayer[$key]['number'] = M('teamuser')->where("uid=".$value['uid']." and teamid=".$res['ateamid'])->getField('playernum');
	 		$aTeamPlayer[$key]['postion'] = $userRes['postion'];
	 		$countRes = M('matchmvp')->where("matchid=$matchid and teamid=".$res['ateamid']." and playeruid=".$value['uid'])->field('count(*) as count')->find();
	 		$aTeamPlayer[$key]['count'] = $countRes['count'];
	 		$countRes = M('matchmvp')->where("matchid=$matchid and teamid=".$res['ateamid']." and playeruid=".$value['uid']." and uid=$uid")->field('count(*) as count')->find();
	 		$aTeamPlayer[$key]['isSupport'] = $countRes['count']==0?'no':'yes';
	 	}
	 	$bTeamPlayer = M('teamuser')->where("isthrough=1 and teamid=".$res['bteamid'])->field('uid')->select();
	 	if (is_null($bTeamPlayer)) {
	 		$bTeamPlayer = array();
	 	}
	 	foreach ($bTeamPlayer as $key => $value){
	 		$userRes = formatUser($value['uid']);
	 		$bTeamPlayer[$key]['nickname'] = $userRes['nickname'];
	 		$bTeamPlayer[$key]['headimg'] = $userRes['headimg'];
	 		$bTeamPlayer[$key]['number'] = M('teamuser')->where("uid=".$value['uid']." and teamid=".$res['bteamid'])->getField('playernum');
	 		$bTeamPlayer[$key]['postion'] = $userRes['postion'];
	 		$countRes = M('matchmvp')->where("matchid=$matchid and teamid=".$res['bteamid']." and playeruid=".$value['uid'])->field('count(*) as count')->find();
	 		$bTeamPlayer[$key]['count'] = $countRes['count'];
	 		$countRes = M('matchmvp')->where("matchid=$matchid and teamid=".$res['bteamid']." and playeruid=".$value['uid']." and uid=$uid")->field('count(*) as count')->find();
	 		$bTeamPlayer[$key]['isSupport'] = $countRes['count']==0?'no':'yes';
	 	}
	 	$return['aTeamPlayer'] = $aTeamPlayer;
	 	$return['bTeamPlayer'] = $bTeamPlayer;
	 	$return['aTeamData'] = $aTeamData;
	 	$return['bTeamData'] = $bTeamData;
	 	get_api_result(200,$return);
	 }

	 /**
	  * mvp投票
	  *@param uid
	  *@param playeruid
	  *@param teamid
	  *@param matchid
	  */
	 public function supportMvp(){
	 	$uid = I('uid',intval);
	 	$playeruid = I('playeruid',intval);
	 	$teamid = I('teamid',intval);
	 	$matchid = I('matchid',intval);
	 	$count = M('matchmvp')->where("uid=$uid and matchid=$matchid")->count();
	 	if ($count!=0) {
	 		get_api_result(302,"你已经投过票了");
	 	}
	 	$data['uid'] = $uid;
	 	$data['playeruid'] = $playeruid;
	 	$data['teamid'] = $teamid;
	 	$data['matchid'] = $matchid;
	 	$res = M('matchmvp')->add($data);
	 	if ($res!=0) {
	 		get_api_result(200,'成功');
	 	}else{
	 		get_api_result(300,'失败');
	 	}
	 }

	 /**
	  * 发布比赛留言
	  *@param matchid
	  *@param uid
	  *@param msg
	  */
	public function publishMatchMsg(){
		$matchid = I('matchid',intval);
		$uid = I('uid',intval);
		$msg = I('msg');
		if ($matchid==0 || $msg=='') {
			get_api_result(301,"参数错误");
		}
		$data['matchid'] = $matchid;
		$data['uid'] = $uid;
		$data['msg'] = $msg;
		$data['addtime'] = time();
		$res = M('matchmsg')->add($data);
		if ($res != 0) {
			$value = formatUser($data['uid']);
			$data['headimg'] = $value['headimg'];
			$data['nickname'] = $value['nickname'];
			$data['addtime'] = $this->_getTimeFormat($data['addtime'],false);
			$data['praiseCount'] = 0;
			$count = M('matchmsgpraise')->where("matchmsgid=".$res." and uid=$uid")->count();
			$data['isPraise'] = 'no';
			$data['matchmsgid'] = $res;
			get_api_result(200,$data);
		}else{
			get_api_result(300,"留言失败");
		}
	}

	/**
	 * 获取比赛留言
	 *@param matchid
	 */
	public function getMatchMsgList(){
		$matchid = I('matchid',intval);
		$matchmsgid = I('matchmsgid',intval);
		$num = I('num',intval);
		$uid = I('uid',intval);
		$where = $matchmsgid==0?"matchmsg.matchid=$matchid":"matchmsg.matchid=$matchid and matchmsg.matchmsgid<$matchmsgid";
		$num = $num==0?5:$num;
		$model = M('matchmsg');
		$totalCount = M('matchmsg')->where("matchid=$matchid")->count();
		$res = $model->join('left join userdata on matchmsg.uid=userdata.uid')->where($where)->field('matchmsg.uid,userdata.nickname,userdata.headimg,matchmsg.msg,matchmsg.addtime,matchmsg.matchmsgid')->order('matchmsg.addtime DESC')->limit($num)->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$res[$key]['headimg'] = httpImg($value['headimg']);
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
			$res[$key]['praiseCount'] = M('matchmsgpraise')->where("matchmsgid=".$value['matchmsgid'])->count();
			$count = M('matchmsgpraise')->where("matchmsgid=".$value['matchmsgid']." and uid=$uid")->count();
			$res[$key]['isPraise'] = $count==0?'no':'yes';
		}
		$data['totalCount'] = $totalCount;
		$data['msg'] = $res;
		get_api_result(200,$data);
	}

	/**
	 * 比赛留言点赞
	 *@param id
	 *@param uid
	 */
	public function matchMsgPraise(){
		$matchmsgid = I('id',intval);
		$uid = I('uid',intval);
		$count = M('matchmsgpraise')->where("matchmsgid=$matchmsgid and uid=$uid")->count();
		if ($count!=0) {
			get_api_result(301,"您已经点过赞了");
		}
		$data['uid'] = $uid;
		$data['matchmsgid'] = $matchmsgid;
		$res = M('matchmsgpraise')->add($data);
		if ($res!=0) {
			get_api_result(200,"点赞成功");
		}else{
			get_api_result(300,"点赞失败");
		}

	}

	/**
	 * 获取比赛数据统计
	 *@param matchleagueid
	 */
	public function getMatchData(){
		$matchleagueid = I('matchleagueid');
		$type = M('matchleague')->where("matchleagueid=$matchleagueid")->getField('type');
		if (is_null($type)) {
			get_api_result(302,"没有这场联赛");
		}
		if ($type==1) {
			$jifenbang = $this->_getZuqiu($matchleagueid);
		}
		if ($type==0) {
			$jifenbang = $this->_getLanqiu($matchleagueid);
		}
		$mvpData = $this->_getMvpList($matchleagueid);
		$totalscorelist = $this->_getGoalList($matchleagueid);
		$totalassistslist = $this->_getAssistsList($matchleagueid);
		$return['jifenbang'] = $jifenbang;
		$return['mvpData'] = $mvpData;
		$return['totalscorelist'] = $totalscorelist;
		$return['totalassistslist'] = $totalassistslist;
		get_api_result(200,$return);
	}

	/**
	 * 获取足球积分榜
	 */
	protected function _getZuqiu($matchleagueid){
		$teamidArr = array();
		$return = array();
		$res = M('match')->where("matchleagueid=$matchleagueid")->field('matchid,ateamid,bteamid,ateamscore,bteamscore,ajifen,bjifen,fenzu')->order("fenzu")->select();
		$res = is_null($res)?array():$res;
		$newres = array();
		$return = array();
		foreach ($res as $key => $value) {
			$newres[$value['fenzu']][] = $value;
		}
		foreach ($newres as $key => $value) {
			$return[$key] = $this->_jiSuanZuQiu($value);
		}
		// dump($return);die;
		return $return;
	}

	protected function _jiSuanZuQiu($res){
		foreach ($res as $key => $value) {
			if (!array_key_exists($value['ateamid'], $teamidArr)) {
				$teamidArr[$value['ateamid']] = array('shen'=>0,'ping'=>0,'fu'=>0,'jsq'=>0,'jifen'=>0);
			}
			if (!array_key_exists($value['bteamid'], $teamidArr)) {
				$teamidArr[$value['bteamid']] = array('shen'=>0,'ping'=>0,'fu'=>0,'jsq'=>0,'jifen'=>0);
			}
		}
		foreach ($res as $key => $value) {
			if ($value['ateamscore'] == '-') {
				continue;
			}
			if ($value['ateamscore'] > $value['bteamscore']) {
				$teamidArr[$value['ateamid']]['shen'] += 1;
				$teamidArr[$value['bteamid']]['fu'] += 1;
			}
			if ($value['ateamscore'] < $value['bteamscore']) {
				$teamidArr[$value['ateamid']]['fu'] += 1;
				$teamidArr[$value['bteamid']]['shen'] += 1;
			}
			if ($value['ateamscore'] == $value['bteamscore']) {
				$teamidArr[$value['ateamid']]['ping'] += 1;
				$teamidArr[$value['bteamid']]['ping'] += 1;
			}

			$teamidArr[$value['ateamid']]['jifen'] += $value['ajifen'];
			$teamidArr[$value['bteamid']]['jifen'] += $value['bjifen'];
			$teamidArr[$value['ateamid']]['jsq'] += ($value['ateamscore'] - $value['bteamscore']);
			$teamidArr[$value['bteamid']]['jsq'] += ($value['bteamscore'] - $value['ateamscore']);
		}
		foreach ($teamidArr as $key => $value) {
			$data[$key] = $value['jifen'];
		}
		asort($data);
		$data = array_reverse($data,true);
		$jifenbang = array();
		foreach ($data as $key => $value) {
			$a['teamid'] = $key;
			$a['name'] = M('team')->where("teamid=$key")->getField('name');
			$a['img'] = httpImg(M('team')->where("teamid=$key")->getField('img'));
			$a = array_merge($a,$teamidArr[$key]);
			$jifenbang[] = $a;
		}
		return $jifenbang;
	}

	/**
	 *获取篮球积分榜
	 */
	protected function _getLanqiu($matchleagueid){
		$teamidArr = array();
		$return = array();
		$res = M('match')->where("matchleagueid=$matchleagueid")->field('matchid,ateamid,bteamid,ateamscore,bteamscore,fenzu')->select();
		$res = is_null($res)?array():$res;
		$newres = array();
		$return = array();
		foreach ($res as $key => $value) {
			$newres[$value['fenzu']][] = $value;
		}
		foreach ($newres as $key => $value) {
			$return[$key] = $this->_jiSuanLanQiu($value);
		}
		// dump($return);die;
		return $return;
		
	}

	protected function _jiSuanLanQiu($res){
		foreach ($res as $key => $value) {
			if (!array_key_exists($value['ateamid'], $teamidArr)) {
				$teamidArr[$value['ateamid']] = array('shen'=>0,'fu'=>0,'lianshen'=>0);
			}
			if (!array_key_exists($value['bteamid'], $teamidArr)) {
				$teamidArr[$value['bteamid']] = array('shen'=>0,'fu'=>0,'lianshen'=>0);
			}
		}
		foreach ($res as $key => $value) {
			if ($value['ateamscore'] == '-') {
				continue;
			}
			if ($value['ateamscore'] > $value['bteamscore']) {
				$teamidArr[$value['ateamid']]['shen'] += 1;
				$teamidArr[$value['bteamid']]['fu'] += 1;
				$teamidArr[$value['ateamid']]['lianshen'] += 1;
				$teamidArr[$value['bteamid']]['lianshen'] = 0;
			}
			if ($value['ateamscore'] < $value['bteamscore']) {
				$teamidArr[$value['ateamid']]['fu'] += 1;
				$teamidArr[$value['bteamid']]['shen'] += 1;
				$teamidArr[$value['ateamid']]['lianshen'] = 0;
				$teamidArr[$value['bteamid']]['lianshen'] += 1;
			}
		}
		foreach ($teamidArr as $key => $value) {
			$data[$key] = $value['shen'];
		}
		asort($data);
		$data = array_reverse($data,true);
		$jifenbang = array();
		foreach ($data as $key => $value) {
			$a['teamid'] = $key;
			$a['name'] = M('team')->where("teamid=$key")->getField('name');
			$a['img'] = httpImg(M('team')->where("teamid=$key")->getField('img'));
			$a = array_merge($a,$teamidArr[$key]);
			$jifenbang[] = $a;
		}
		return $jifenbang;
	}


	/**
	 * 获取mvp排行
	 */
	protected function _getMvpList($matchleagueid){
		$res = M('match')->where("matchleagueid=$matchleagueid")->field('matchid,ateamid,bteamid,ateamscore,bteamscore')->select();
		$res = is_null($res)?array():$res;
		$mvpList = array();
		$teamList = array();
		$mvpData = array();
		foreach ($res as $key => $value) {
			$mvpRes = M('matchmvp')->where('matchid='.$value['matchid'])->field('playeruid,teamid,count(*) as count')->order('count DESC')->find();
			if($mvpRes['playeruid']==0){
				continue;
			}
			if (!array_key_exists($mvpRes['playeruid'], $mvpList)) {
				$mvpList[$mvpRes['playeruid']] = 1;
				$teamList[$mvpRes['playeruid']] = $mvpRes['teamid'];
			}else{
				$mvpList[$mvpRes['playeruid']] += 1;
			}
			unset($mvpRes);
		}
		asort($mvpList);
		$mvpList = array_reverse($mvpList,true);
		foreach ($mvpList as $key => $value) {
			$mvp['uid'] = $key;
			$userData = formatUser($key);
			$mvp['nickname'] = $userData['nickname'];
			$mvp['headimg'] = $userData['headimg'];
			$mvp['sex'] = $userData['sex'];
			$mvp['postion'] = $userData['postion'];
			$mvp['teamname'] = M('team')->where("teamid=".$teamList[$key])->getField('name');
			$mvp['role'] = M('teamuser')->where("teamid=".$teamList[$key]." and uid=$key")->getField('role');
			$mvp['mvpcount'] = $value;
			$mvpData[] = $mvp;
		}
		return $mvpData;
	}


	/**
	 * 获取总得分榜
	 */
	protected function _getGoalList($matchleagueid){
		$model = new Model();
		$res = $model->query("select md2.uid,md2.teamid,sum(md2.goal) as totalscore from `match` m left join matchdata md on (m.ateamid=md.teamid  and m.matchid=md.matchid) or (m.bteamid=md.teamid and m.matchid=md.matchid) right join matchdata md2 on md.matchid=md2.matchid and md.teamid=md2.teamid and md.uid=md2.uid where m.matchleagueid=$matchleagueid group by md2.uid order by totalscore DESC");
		foreach ($res as $key => $value) {
			$res[$key]['teamname'] = M('team')->where("teamid=".$value['teamid'])->getField('name');
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['sex'] = $userData['sex'];
			$res[$key]['postion'] = $userData['postion'];
			$res[$key]['role'] = M('teamuser')->where("teamid=".$value['teamid']." and uid=".$value['uid'])->getField('role');
		}
		return $res;
	}

	/**
	 * 总助攻榜
	 */
	protected function _getAssistsList($matchleagueid){
		$model = new Model();
		$res = $model->query("select md2.uid,md2.teamid,sum(md2.assists) as totalassists from `match` m left join matchdata md on (m.ateamid=md.teamid  and m.matchid=md.matchid) or (m.bteamid=md.teamid and m.matchid=md.matchid) right join matchdata md2 on md.matchid=md2.matchid and md.teamid=md2.teamid and md.uid=md2.uid where m.matchleagueid=$matchleagueid group by md2.uid order by totalassists DESC");
		foreach ($res as $key => $value) {
			$res[$key]['teamname'] = M('team')->where("teamid=".$value['teamid'])->getField('name');
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['headimg'] = $userData['headimg'];
			$res[$key]['sex'] = $userData['sex'];
			$res[$key]['postion'] = $userData['postion'];
			$res[$key]['role'] = M('teamuser')->where("teamid=".$value['teamid']." and uid=".$value['uid'])->getField('role');
		}
		return $res;
	}

	/**
	 * 获取我的比赛列表
	 *@param uid
	 */
	public function getMyMatch(){
		$uid = I('uid');
		$isdone = I('isdone',intval);
		$time = time();
		$model = new Model();
		$res = $model->query("select distinct ml.name,ml.location,ml.type,m.ateamid,m.bteamid,m.matchid,at.name as ateamname,at.img as ateamimg,bt.name as bteamname,bt.img as bteamimg,m.date,m.ateamscore,m.bteamscore,m.date,m.matchlocation from `match` m right join teamuser tu on m.ateamid=tu.teamid or m.bteamid=tu.teamid left join team at on at.teamid=m.ateamid left join team bt on bt.teamid=m.bteamid left join matchleague ml on ml.matchleagueid=m.matchleagueid where tu.uid=$uid and tu.isthrough=1");
		$res = is_null($res)?array():$res;
		$re = array();
		foreach ($res as $key => $value){
			$res[$key]['ateamimg'] = httpImg($value['ateamimg']);
			$res[$key]['bteamimg'] = httpImg($value['bteamimg']);
			if (is_null($value['name'])) {
				unset($res[$key]);
			}else{
				if ($isdone == 0) {
					if ($value['date'] >= $time) {
						$re[] = $res[$key];
					}
				}else{
					if ($value['date'] < $time) {
						$re[] = $res[$key];
					}
				}
			}
		}
		get_api_result(200,$re);
	}

	/**
	 * 获取地区数据
	 */
	public function getAreaList(){
		$res = M('area')->select();
		get_api_result(200,$res);
	}


	/**
	 * 比赛开始前推送
	 */
	public function pushBeforMatch(){
		$time = time();
		$befor = C('befor');
		$matchDate = $time + $befor;
		$res = M('match')->where("date>$time and date<$matchDate")->field("ateamid,bteamid")->select();
		foreach ($res as $key => $value) {
			$playerDataA = M('teamuser')->where("teamid=".$value['ateamid']." and isthrough=1")->field('uid')->select();
			$playerDataB = M('teamuser')->where("teamid=".$value['bteamid']." and isthrough=1")->field('uid')->select();
			$playerData = array_merge($playerDataA,$playerDataB);
			foreach ($playerData as $value) {
				push($value['uid'],3);
			}
		}
		echo time();
		var_dump($res);
	}

		/**
	 * 格式化时间
	 *第二个参数false时为时间戳 
	 */
	private function _getTimeFormat($the_time, $isString)
    {
        if ($isString)
        {
            $the_time = strtotime($the_time);
        }
        $show_time = date("m-d H:i", $the_time);
        $today = strtotime('today');
        $yesterday = $today - 24*3600;
        $durToday = time() - $today;
        $durYesterday = time() - $yesterday;
        $dur = time() - $the_time;
        if($dur < 0){
            return $show_time;
        }
        else
        {
            if($dur < 60)
            {
                return $dur.'秒前';
            }
            else
            {
                if($dur < 3600)
                {
                    return floor($dur/60).'分钟前';
                }
                else
                {
                    if($dur < 10800)
                    {
                        return floor($dur/3600).'小时前';
                    }
                    else
                    {
                        if($dur < $durToday)
                        {//3天内
                            return '今天 '.date('H:i', $the_time);
                        }
                        elseif ($dur < $durYesterday)
                        {
                            return '昨天 '.date('H:i', $the_time);
                        }
                        else
                        {
                            return $show_time;
                        }
                    }
                }
            }
        }
    }
}


 ?>