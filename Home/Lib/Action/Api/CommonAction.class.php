<?php 
/**
* 
*/
class CommonAction extends Action
{
	
	public function _initialize(){
		if (isset($_GET['uid'])||isset($_POST['uid'])) {
			$uid = I('uid');
			$count = M('user')->where("uid=$uid")->count();
			if ($count==0) {
				get_api_result(309,'用户不存在');
			}
		}
        if (isset($_GET['teamid'])||isset($_POST['teamid'])) {
            $teamid = I('teamid');
            $count = M('team')->where("teamid=$teamid")->count();
            if ($count==0) {
                get_api_result(310,'球队不存在');
            }
        }
        if (isset($_GET['ycdid'])||isset($_POST['ycdid'])) {
            $ycdid = I('ycdid');
            if ($ycdid!=0) {
                $count = M('ycd')->where("ycdid=$ycdid")->count();
                if ($count==0) {
                    get_api_result(311,'该场地不存在');
                }
            }
        }
	}

	private function _getDataFromPost($url, $params)
    {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $data = curl_exec($ch); 
        curl_close($ch);
        return $data;
    }

     public function getDataPost($method, $params, $getCode = false)
    {
        $request = '';
        $request = implode('/', $method);
        $request = C('sms_interfaceUrl').$request;

        $tmp = '';
        $tmp = $this->_getDataFromPost($request, $params);
        if (!$tmp)
        {

            return false;
        }
        $ob= simplexml_load_string($tmp);
		$json  = json_encode($ob);
		$data = json_decode($json, true); 
        return $data;
    }

    private function _getDataFromUrl($url)
    {   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        $data = curl_exec($ch); 
        curl_close($ch);
        return $data;
    }

    public function getData($method, $params, $getCode = false)
    {
        $request = '';
        $request = implode('/', $method);
        $paramsArr = array();
        foreach ($params as $key => $value) {
            $paramsArr[] = $key.'='.$value;
        }
        $request .= '?'.implode('&', $paramsArr);
        $request = C('sms_interfaceUrl').$request;

        $tmp = '';
        $tmp = $this->_getDataFromUrl($request);
        if (!$tmp)
        {
            zlog($request);
            return false;
        }
        $data = json_decode($tmp, true);
        return $data;
    }


}




 ?>