<?php 
/**
* 朋友圈模块
*/
class PyqAction extends CommonAction
{
	
	/**
	 * 发布朋友圈内容
	 *@param uid
	 *@param content
	 *@param img (可选参数)
	 */
	public function publishMsg(){
		$uid = I('uid');
		$content = I('content');
		$img = unzip(C('pyqImgZipPath'),C('pyqImgPath'));
		$model = D('Pyq');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}
		$model->startTrans();
		$res1 = $model->add($data);
		if ($img != false) {
			foreach ($img as $key => $value) {
				$a['pyqid'] = $res1;
				$a['img'] = $value;
				$dataImg[] = $a;
			}
			$res2 = M('pyqimg')->addAll($dataImg);
			if ($res1!=0 && $res2!=0) {
				$model->commit();
				get_api_result(200,array('pyqid'=>$res1));
			}else{
				echo $model->getError();
				$model->rollback();
				get_api_result(300,"发布失败~");
			}
		}
		if ($res1!=0) {
				$model->commit();
				get_api_result(200,array('pyqid'=>$res1));
			}else{
				$model->rollback();
				get_api_result(300,"发布失败~");
			}
	}

	/**
	 * 获取朋友圈信息列表
	 *@param uid
	 *@param pyqid
	 */
	public function getPyqList(){
		$uid = I('uid',intval);
		$pyqid = I('pyqid');
		$num = I('num',intval)==0?5:I('num',intval);
		if ($pyqid==0){
			$where = "at.uid=$uid"." or p.uid=$uid";
		}else{
			$where = "(at.uid=$uid and p.pyqid<$pyqid)"." or (p.uid=$uid and p.pyqid<$pyqid)";
		}
		$model = M('pyq p');
		$res = $model->join('left join attention at on at.touid=p.uid')->where($where)->field('distinct p.*')->order('p.addtime DESC')->limit($num)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$img = M('pyqimg')->where("pyqid=".$value['pyqid'])->field('img')->select();
			$img = is_null($img)?array():$img;
			foreach ($img as $k => $v) {
				$img[$k]['img'] = httpImg($v['img']);
			}
			$res[$key]['img'] = $img;
			$res[$key]['plcount'] = M('pyqpl')->where("pyqid=".$value['pyqid'])->count();
			$res[$key]['zhancount'] = M('pyqzhan')->where("pyqid=".$value['pyqid'])->count();
			$zhanList = M('pyqzhan')->where("pyqid=".$value['pyqid'])->field('uid')->select();
			$zhanList = is_null($zhanList)?array():$zhanList;
			foreach ($zhanList as $k1 => $v1) {
				$ud = formatUser($v1['uid']);
				$zhanList[$k1]['nickname'] = $ud['nickname'];
				$zhanList[$k1]['sex'] = $ud['sex'];
			}
			$res[$key]['zhanlist'] = $zhanList;
			$res[$key]['isZhan'] = M('pyqzhan')->where("pyqid=".$value['pyqid']." and uid=$uid")->count();
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
		}
		get_api_result(200,$res);
	}

	/**
	 * 朋友圈点赞
	 *@param pyqid
	 *@param uid
	 */
	public function pyqZhan(){
		$uid = I('uid');
		$pyqid = I('pyqid');
		$count = M('pyqzhan')->where("pyqid=$pyqid and uid=$uid")->count();
		if ($count!=0) {
			get_api_result(302,"你已经点过赞了~");
		}
		$data['uid'] = $uid;
		$data['pyqid'] = $pyqid;
		$res = M('pyqzhan')->add($data);
		if ($res!=0) {
			get_api_result(200,"点赞成功~");
		}else{
			get_api_result(300,"点赞失败~");
		}
	}

	/**
	 * 发布朋友圈评论
	 *@param uid
	 *@param pyqid
	 *@param content
	 */
	public function publishComment(){
		$model = D('Pyqpl');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res!=0) {
				$puid = M('pyq')->where("pyqid=".I('pyqid'))->getField('uid');
				push($puid,0);
				$data['pyqplid'] = $res;
				get_api_result(200,array('pyqplid'=>$data));
			}else{
				get_api_result(300,"评论失败~");
			}
		}
	}

	/**
	 * 获取朋友圈评论列表
	 *@param pyqid
	 *@param pyqplid
	 *@param num
	 */
	public function getCommentList(){
		$pyqid = I('pyqid',intval);
		$num = I('num')==0?5:I('num');
		$pyqplid = I('pyqplid',intval);
		if ($pyqplid == 0) {
			$where = "pyqid=$pyqid";
		}else{
			$where = "pyqid=$pyqid and pyqplid>$pyqplid";
		}
		$res = M('pyqpl')->where($where)->limit($num)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime']);
			$plhfData = M('pyqplhf')->where('pyqplid='.$value['pyqplid'])->select();
			foreach ($plhfData as $k => $v) {
				$ud1 = formatUser($v['uid']);
				$plhfData[$k]['nickname'] = $ud1['nickname'];
				$plhfData[$k]['sex'] = $ud1['sex'];				
				$ud1 = formatUser($v['hfuid']);
				$plhfData[$k]['hfnickname'] = $ud1['nickname'];
				$plhfData[$k]['hfsex'] = $ud1['sex'];				
			}
			$res[$key]['hfdata'] = $plhfData;
		}
		get_api_result(200,$res);
	}

	/**
	 * 朋友圈评论回复
	 *@param uid
	 *@param hfuid
	 *@param pyqplid
	 *@param content
	 */
	public function publishPyqMsgPlHf(){
		$uid = I('uid');
		$model = D('Pyqplhf');
		$data = $model->create();
		if (!$model->create()) {
			get_api_result(301,$model->getError());
		}else{
			$res = $model->add();
			if ($res !=0 ) {
				push(I('hfuid'),1);
				$data['pyqplhfid'] = $res;
				$ud = formatUser($data['uid']);
				$hfud = formatUser($data['hfuid']);
				$data['nickname'] = $ud['nickname'];
				$data['sex'] = $ud['sex'];
				$data['hfnickname'] = $hfud['nickname'];
				$data['hfsex'] = $hfud['sex'];
				get_api_result(200,$data);
			}else{
				get_api_result(300,"失败");
			}
		}
	}

	/**
	 * 获取朋友圈同城列表
	 *@param location
	 *@param uid
	 *@param pyqid
	 *@param num
	 */
	public function getCityPyq(){
		$location = I('location');
		$uid = I('uid',intval);
		$pyqid = I('pyqid',intval);
		$num = I('num',intval)==0?5:I('num',intval);
		if ($pyqid==0) {
			$where = "location='$location'";
		}else{
			$where = "location='$location' and pyqid<$pyqid";
		}
		$model = M('pyq');
		$res = $model->where($where)->field('distinct *')->order('addtime DESC')->limit($num)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$img = M('pyqimg')->where("pyqid=".$value['pyqid'])->field('img')->select();
			$img = is_null($img)?array():$img;
			foreach ($img as $k => $v) {
				$img[$k]['img'] = httpImg($v['img']);
			}
			$res[$key]['img'] = $img;
			$res[$key]['plcount'] = M('pyqpl')->where("pyqid=".$value['pyqid'])->count();
			$res[$key]['zhancount'] = M('pyqzhan')->where("pyqid=".$value['pyqid'])->count();
			$zhanList = M('pyqzhan')->where("pyqid=".$value['pyqid'])->field('uid')->select();
			$zhanList = is_null($zhanList)?array():$zhanList;
			foreach ($zhanList as $k1 => $v1) {
				$ud = formatUser($v1['uid']);
				$zhanList[$k1]['nickname'] = $ud['nickname'];
				$zhanList[$k1]['sex'] = $ud['sex'];
			}
			$res[$key]['zhanlist'] = $zhanList;
			$res[$key]['isZhan'] = M('pyqzhan')->where("pyqid=".$value['pyqid']." and uid=$uid")->count();
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
		}
		get_api_result(200,$res);
	}

	/**
	 * 随便看看
	 *@param uid
	 *@param num
	 */
	public function getRandPyq(){
		$uid = I('uid',intval);
		$num = I('num',intval)==0?5:I('num',intval);
		$model = M('pyq');
		$res = $model->field('*,rand() as rand')->order('rand DESC')->limit($num)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$ud = formatUser($value['uid']);
			$res[$key]['nickname'] = $ud['nickname'];
			$res[$key]['headimg'] = $ud['headimg'];
			$res[$key]['sex'] = $ud['sex'];
			$img = M('pyqimg')->where("pyqid=".$value['pyqid'])->field('img')->select();
			$img = is_null($img)?array():$img;
			foreach ($img as $k => $v) {
				$img[$k]['img'] = httpImg($v['img']);
			}
			$res[$key]['img'] = $img;
			$res[$key]['plcount'] = M('pyqpl')->where("pyqid=".$value['pyqid'])->count();
			$res[$key]['zhancount'] = M('pyqzhan')->where("pyqid=".$value['pyqid'])->count();
			$zhanList = M('pyqzhan')->where("pyqid=".$value['pyqid'])->field('uid')->select();
			$zhanList = is_null($zhanList)?array():$zhanList;
			foreach ($zhanList as $k1 => $v1) {
				$ud = formatUser($v1['uid']);
				$zhanList[$k1]['nickname'] = $ud['nickname'];
				$zhanList[$k1]['sex'] = $ud['sex'];
			}
			$res[$key]['zhanlist'] = $zhanList;
			$res[$key]['isZhan'] = M('pyqzhan')->where("pyqid=".$value['pyqid']." and uid=$uid")->count();
			$res[$key]['addtime'] = $this->_getTimeFormat($value['addtime'],false);
		}
		get_api_result(200,$res);
	}

	/**
	 * 朋友圈消息推送
	 */
	public function pushMsg(){
		$idarr = I('idarr');
		$idarr = explode(',', $idarr);
		foreach ($idarr as $value) {
			if ($value == '') continue;
			echo $value.">>>>>";
			var_dump(push($value,2));
		}
		echo "xxxx";
	}


	/**
	 * 格式化时间
	 *第二个参数false时为时间戳 
	 */
	private function _getTimeFormat($the_time, $isString)
    {
        if ($isString)
        {
            $the_time = strtotime($the_time);
        }
        $show_time = date("m-d H:i", $the_time);
        $today = strtotime('today');
        $yesterday = $today - 24*3600;
        $durToday = time() - $today;
        $durYesterday = time() - $yesterday;
        $dur = time() - $the_time;
        if($dur < 0){
            return $show_time;
        }
        else
        {
            if($dur < 60)
            {
                return $dur.'秒前';
            }
            else
            {
                if($dur < 3600)
                {
                    return floor($dur/60).'分钟前';
                }
                else
                {
                    if($dur < 10800)
                    {
                        return floor($dur/3600).'小时前';
                    }
                    else
                    {
                        if($dur < $durToday)
                        {//3天内
                            return '今天 '.date('H:i', $the_time);
                        }
                        elseif ($dur < $durYesterday)
                        {
                            return '昨天 '.date('H:i', $the_time);
                        }
                        else
                        {
                            return $show_time;
                        }
                    }
                }
            }
        }
    }



}




 ?>