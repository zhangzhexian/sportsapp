<?php 
/**
* 球队管理
*/
class TeamAction extends CommonAction
{
	
	/**
	 * 查看球队列表
	 */
	public function teamList(){
		$num = I('num',intval)==0?20:I('num',intval);
		$type = I('type',intval);
    	$model = M('team');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();
		$res = M('team')->where('type='.$type)->limit($Page->firstRow.','.$Page->listRows)->select();
		foreach ($res as $key => $value) {
			$nickname = M('teamuser tu')->join('left join userdata ud on tu.uid=ud.uid')->where("tu.teamid=".$value['teamid']." and tu.role=2")->getField('ud.nickname');
			$res[$key]['nickname'] = is_null($nickname)?'':$nickname;
			$tel = M('teamuser tu')->join('left join user u on tu.uid=u.uid')->where("tu.teamid=".$value['teamid']." and tu.role=2")->getField('u.tel');
			$res[$key]['tel'] = is_null($tel)?'':$tel;
			$res[$key]['count'] = M('teamuser')->where("teamid=".$value['teamid']." and isthrough=1")->count();
			$res[$key]['imgcatecount'] = M('teamimgcate')->where("teamid=".$value['teamid'])->count();
		}
		$this->res = $res;
		$this->page = $show;
		$this->display();
	}

	/**
	 * 删除球队
	 *@param teamid
	 */
	public function delTeam(){
		$teamid = I('teamid',intval);
		$type = I('type',intval);
		$model = M('team');
		$count = M('match')->where("ateamid=$teamid or bteamid=$teamid")->count();
		if ($count != 0) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("请先删除包含此球队的比赛"); </script>';die;
		}
		$model->startTrans();
		$res1 = M('team')->where('teamid='.$teamid)->delete();
		$res2 = M('teamuser')->where('teamid='.$teamid)->delete();
		if ($res1!=0&&$res2!=0) {
			$model->commit();
			redirect(U('Home/Team/teamList',array('type'=>$type)));
		}else{
			$model->rollback();
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

	/**
	 * 球队成员列表
	 *@param teamid
	 */
	public function teamUserList(){
		$teamid = I('teamid',intval);
		$res = M('teamuser tu')->where("tu.teamid=".$teamid." and tu.isthrough=1")->join('left join userdata ud on tu.uid=ud.uid left join user u on tu.uid=u.uid')->field('tu.*,ud.nickname,u.tel')->order('tu.role DESC')->select();
		foreach ($res as $key => $value) {
			switch ($value['role']) {
				case '0':
					$res[$key]['role'] = '队员';
					break;
				case '1':
					$res[$key]['role'] = '副队长';
					break;
				case '2':
					$res[$key]['role'] = '队长';
					break;
			}
		}
		$this->res = $res;
		$this->display();
	}

	/**
	 * 球队相册列表
	 *@param teamid
	 */
	public function teamImgCateList(){
		$teamid = I('teamid',intval);
		$res = M('teamimgcate')->where("teamid=$teamid")->order('addtime DESC')->select();
		// var_dump($res);
		foreach ($res as $key => $value) {
			$res[$key]['count'] = M('teamimg')->where("teamimgcateid=".$value['teamimgcateid'])->count();
		}
		$this->res = $res;
		$this->display();
	}

	/**
	 * 删除球队相册
	 *@param teamimgcateid
	 */
	public function delTeamImgCate(){
		$teamimgcateid = I('teamimgcateid',intval);
		$teamid = I('teamid',intval);
		$res = M('teamimgcate')->where("teamimgcateid=$teamimgcateid")->delete();
		if ($res!=0) {
			redirect(U('Home/Team/teamImgCateList',array('teamid'=>$teamid)));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

	/**
	 * 球队相册列表
	 *@param teamimgcateid
	 */
	public function teamImgList(){
		$teamimgcateid = I('teamimgcateid',intval);
		$res = M('teamimg')->where("teamimgcateid=$teamimgcateid")->select();
		// var_dump($res);
		$this->res = $res;
		$this->display();
	}
}



 ?>