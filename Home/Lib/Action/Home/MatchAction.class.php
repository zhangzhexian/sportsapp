<?php 
/**
* 比赛管理
*/
class MatchAction extends CommonAction
{
	/**
	 * 联赛列表
	 */
	public function matchLeagueList(){
		$type = I('type');
		$res = M('matchleague')->where("type=$type")->order('addtime DESC')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$res[$key]['matchCount'] = M('match')->where("matchleagueid=".$value['matchleagueid'])->count();
			$res[$key]['newsCount'] = M('news')->where("matchleagueid=".$value['matchleagueid'])->count();
		}
		$this->res = $res;
		$this->display();
	}
	/**
	 * 添加联赛
	 *@param type
	 */
	public function addMatchLeague(){
		$this->display();
	}


	/**
	 * 添加联赛处理
	 */
	public function addMatchLeagueHandle(){
		$type = I('type');
		$model = D('Matchleague');
		$_POST['img'] = uploadFormImg('img',C('matchLeagueImg'));
		// var_dump($data);die;
		if (!$model->create()) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
		}else{
			$res = $model->add();
			if ($res != 0) {
				redirect(U('Home/Match/matchLeagueList',array('type'=>$type)));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
			}
		}
	}

	/**
	 * 删除联赛
	 *@param matchleagueid
	 */
	public function delMatchLeague(){
		$type = I('type');
		$matchleagueid = I('matchleagueid',intval);
		$res = M('matchleague')->where("matchleagueid=$matchleagueid")->delete();
		if ($res != 0) {
			redirect(U('Home/Match/matchLeagueList',array('type'=>$type)));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

	/**
	 * 比赛列表
	 *@param matchleagueid
	 */
	public function matchList(){
		$type = I('type');
		$matchleagueid = I('matchleagueid',intval);
		$res = M('match')->where("matchleagueid=$matchleagueid")->order('fenzu ASC,date DESC')->select();
		if (is_null($res)) {
			$res = array();
		}
		foreach ($res as $key => $value) {
			$res[$key]['ateamname'] = M('team')->where("teamid=".$value['ateamid'])->getField('name');
			$res[$key]['bteamname'] = M('team')->where("teamid=".$value['bteamid'])->getField('name');
		}
		// var_dump($res);
		$this->res = $res;
		$this->display();
	}

	/**
	 * 添加比赛
	 *@param type
	 *@param matchdata
	 */
	public function addMatch(){
		$matchleagueid = $_GET['matchleagueid'];
		$type = $_GET['type'];
		$excel = $_FILES['matchdata'];
	    $extArr = explode('.', $excel['name']);
	    $ext = array_pop($extArr);
	    if (!($ext=="xls"||$ext=="xlsx")) {
	      echo "请上传一个excel文件";die;
	    }
	    /**对excel里的日期进行格式转化*/
	    $filePath = $excel['tmp_name'];
	    include './Ext/PHPExcel.php';
	    $PHPExcel = new PHPExcel(); 

	    /**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/ 
	    $PHPReader = new PHPExcel_Reader_Excel2007(); 
	    if(!$PHPReader->canRead($filePath)){ 
	        $PHPReader = new PHPExcel_Reader_Excel5(); 
	        if(!$PHPReader->canRead($filePath)){ 
	            echo 'no Excel'; 
	            return ; 
	        } 
	    } 

	    $PHPExcel = $PHPReader->load($filePath); 
	    /**读取excel文件中的第一个工作表*/ 
	    $currentSheet = $PHPExcel->getSheet(0); 
	    /**取得最大的列号*/ 
	    $allColumn = $currentSheet->getHighestColumn();
	    if ($allColumn > 'E') {
	       echo "excel格式不正确";die;
	     } 
	    /**取得一共有多少行*/ 
	    $allRow = $currentSheet->getHighestRow();
	    $row = array();
	       /**从第二行开始输出，因为excel表中第一行为列名*/ 
	        for($currentRow = 2;$currentRow <= $allRow;$currentRow++){ 
	            /**从第A列开始输出*/
	            $a= array();
	            $v = $currentSheet->getCellByColumnAndRow(ord('A') - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 
	                /**如果输出汉字有乱码，则需将输出内容用iconv函数进行编码转换，如下将gb2312编码转为utf-8编码输出*/ 
	                $va =  iconv('utf-8','gb2312', $v);
	            if (empty($va)) {
	            	continue;
	            }
	            for($currentColumn= 'A';$currentColumn<= $allColumn; $currentColumn++){ 
	                $val = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 
	                /**如果输出汉字有乱码，则需将输出内容用iconv函数进行编码转换，如下将gb2312编码转为utf-8编码输出*/ 
	                $value =  iconv('utf-8','gb2312', $val);
	                switch ($currentColumn) {
	                      case 'A':
	                          $a['date'] = strtotime($value);
	                          break;
	                      case 'B':
	                          $a['matchlocation'] = mb_convert_encoding ($value, "UTF-8", "gb2312");
	                          break;
	                      case 'C':
	                      	  $aname = mb_convert_encoding ($value, "UTF-8", "gb2312");
	                      	  $aid = M('team')->where("name like '".$aname."'")->getField('teamid');
	                      	  if (is_null($aid)) {
	                      	  	echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("球队>'.$aname.$currentRow.$currentColumn.'<不存在"); </script>';die;
	                      	  }
	                      	  $a['ateamid'] = $aid;	                          
	                          break;
	                      case 'D':
	                          $bname = mb_convert_encoding ($value, "UTF-8", "gb2312");
	                      	  $bid = M('team')->where("name like '".$bname."'")->getField('teamid');
	                      	  if (is_null($bid)) {
	                      	  	echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("球队>'.$aname.$currentRow.$currentColumn.'<不存在"); </script>';die;
	                      	  }
	                      	  $a['bteamid'] = $bid;	                          
	                          break;
	                       case 'E':
	                          $fenzu = mb_convert_encoding ($value, "UTF-8", "gb2312");	                      	  
	                      	  $a['fenzu'] = $fenzu;	                          
	                          break;
	                }
	               
	            }
	            $row[]=$a; 
	        }
	        foreach ($row as $key => $value) {
	        	$row[$key]['matchleagueid'] = $matchleagueid;
	        	$row[$key]['addtime'] = time();
	        }
	        $res = M('match')->addAll($row);
	        if ($res != 0) {
				redirect(U('Home/Match/matchList',array('type'=>$type,'matchleagueid'=>$matchleagueid)));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
			}
     }

     /**
      * 删除比赛
      *@param matchid
      *@param type
      *@param matchleagueid
      */
     public function delMatch(){
     	$matchid = I('matchid');
     	$type = I('type');
     	$matchleagueid = I('matchleagueid');
     	$res = M('match')->where("matchid=$matchid")->delete();
     	if ($res != 0) {
				redirect(U('Home/Match/matchList',array('type'=>$type,'matchleagueid'=>$matchleagueid)));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
			}

     }

     /**
      * 上传比赛结果
      *@param matchid
      *@param matchdata
      */
     public function addMatchResultData(){
     	// var_dump($_FILES);die;
     	$matchid = I('matchid');
     	$type = $_GET['type'];
		$excel = $_FILES['matchdata'];
	    $extArr = explode('.', $excel['name']);
	    $ext = array_pop($extArr);
	    if (!($ext=="xls"||$ext=="xlsx")) {
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert(""请上传一个excel文件"); </script>';die;
	    }
	    /**对excel里的日期进行格式转化*/
	    $filePath = $excel['tmp_name'];
	    include './Ext/PHPExcel.php';
	    $PHPExcel = new PHPExcel(); 

	    /**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/ 
	    $PHPReader = new PHPExcel_Reader_Excel2007(); 
	    if(!$PHPReader->canRead($filePath)){ 
	        $PHPReader = new PHPExcel_Reader_Excel5(); 
	        if(!$PHPReader->canRead($filePath)){ 
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert(""请上传一个excel文件"); </script>';die;
	        } 
	    } 

	    $PHPExcel = $PHPReader->load($filePath); 
	    /**读取excel文件中的第一个工作表*/ 
	    $currentSheet = $PHPExcel->getSheet(0); 
	    /**取得最大的列号*/ 
	    $allColumn = $currentSheet->getHighestColumn();
	    if ($allColumn > 'D') {
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert(""请上传一个excel文件"); </script>';die;
	     } 
	    /**取得一共有多少行*/ 
	    $allRow = $currentSheet->getHighestRow();
	    $row = array();
	       /**从第二行开始输出，因为excel表中第一行为列名*/ 
	        for($currentRow = 2;$currentRow <= $allRow;$currentRow++){
	        	if ($currentRow == 3) continue;
	        	if ($currentRow == 2) {
	        		$score = array();
		        	 for($currentColumn= 'A';$currentColumn<= $allColumn; $currentColumn++){ 
		                $val = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 
		                /**如果输出汉字有乱码，则需将输出内容用iconv函数进行编码转换，如下将gb2312编码转为utf-8编码输出*/ 
		                $value =  iconv('utf-8','gb2312', $val);
		                switch ($currentColumn) {
		                      case 'A':
		                          $score['ateam'] = mb_convert_encoding ($value, "UTF-8", "GBK");
		                          break;
		                      case 'B':
		                          $score['bteam'] = mb_convert_encoding ($value, "UTF-8", "GBK");
		                          break;
		                      case 'C':
		                          $score['ajifen'] = mb_convert_encoding ($value, "UTF-8", "GBK");
		                          break;
		                      case 'D':
		                          $score['bjifen'] = mb_convert_encoding ($value, "UTF-8", "GBK");
		                          break;
		                }
		               
		            }
	       		 }else{
		            /**从第A列开始输出*/
		            $matchData= array();
		            for($currentColumn= 'A';$currentColumn<= $allColumn; $currentColumn++){ 
		                $val = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow)->getValue();/**ord()将字符转为十进制数*/ 
		                /**如果输出汉字有乱码，则需将输出内容用iconv函数进行编码转换，如下将gb2312编码转为utf-8编码输出*/ 
		                $value =  iconv('utf-8','gb2312', $val);
		                switch ($currentColumn) {
		                      case 'A':
		                      		$aname = mb_convert_encoding ($value, "UTF-8", "gb2312");
			                      	  $aid = M('team')->where("name like '".$aname."'")->getField('teamid');
			                      	  if (is_null($aid)) {
			                      	  	echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("球队>'.$aname.$currentRow.$currentColumn.'<不存在"); </script>';die;
			                      	  }
		                          $matchData['teamid'] = $aid;
		                          break;
		                      case 'B':
		                          $matchData['playernum'] =$value;
		                          break;
		                      case 'C':
		                          $matchData['goal'] =$value;
		                          break;
		                      case 'D':
		                          $matchData['assists'] = $value;
		                          break;
		                }
		               
		            }
		            $row[]=$matchData;
		         }    
	        }
	        $model = M('match');
	        $model->startTrans();
	        $res1 = M('match')->where("matchid=$matchid")->save(array('ateamscore'=>$score['ateam'],'bteamscore'=>$score['bteam'],'ajifen'=>$score['ajifen'],'bjifen'=>$score['bjifen']));
	        foreach ($row as $key => $value) {
	        	$playeruid = M('teamuser')->where("teamid=".$value['teamid']." and playernum=".$value['playernum'])->getField('uid');
	        	if (is_null($playeruid)) {
	        		$model->rollback();
					echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("球队号'.$value['teamid'].'中球员号'.$value['playernum'].'不存在"); </script>';die;
	        	}else{
	        		$row[$key]['uid'] = $playeruid;
	        		$row[$key]['matchid'] = $matchid;
	        		unset($row[$key]['playernum']);
	        	}
	        }
	        $res2 = M('matchdata')->addAll($row);
	        if ($res1!=0 && $res2!=0) {
	        	$model->commit();
	        	echo '<script  language="javascript" type="text/javascript">window.history.back(-1);window.location.reload(true);alert("成功"); </script>';die;
	        }else{
	        	$model->rollback();
	        	echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';die;
	        }
     }

     /**
      * 比赛数据列表
      *@param matchid
      */
     public function matchResultData(){
     	$matchid = I('matchid');
     	$teamIdArr = M('match')->where('matchid='.$matchid)->field('ateamid,bteamid')->find();
     	$ateamData = $this->_formatMatchResultData($matchid,$teamIdArr['ateamid']);
     	$bteamData = $this->_formatMatchResultData($matchid,$teamIdArr['bteamid']);
     	$ateamname = M('team')->where("teamid=".$teamIdArr['ateamid'])->getField('name');
     	$bteamname = M('team')->where("teamid=".$teamIdArr['bteamid'])->getField('name');
     	$this->ateamname = $ateamname;
     	$this->bteamname = $bteamname;
     	$this->ateamData = $ateamData;
     	$this->bteamData = $bteamData;
     	$this->display();
     }

     protected function _formatMatchResultData($matchid,$teamid){
     	$res = M('matchdata')->where("teamid=$teamid and matchid=$matchid")->select();
     	if (is_null($res)) {
     		$res = array();
     	}
     	foreach ($res as $key => $value) {
     		$res[$key]['nickname'] = M('userdata')->where('uid='.$value['uid'])->getField('nickname');
     		$res[$key]['playernum'] = M('teamuser')->where('uid='.$value['uid'].' and teamid='.$teamid)->getField('playernum');
     	}
     	return $res;
     }


     /**
      * excel模版下载
      *@param type    0比赛模版   1比赛数据模版
      */
     public function excelDownload(){
     	$type = I('type');
     	if ($type==0) {
     		$file_name="TianJiaBiSai.xls"; 
     	}
     	if ($type==1) {
     		$file_name="TianJiaBiSaiJieGuo.xls"; 
     	}
     	header("Content-type:text/html;charset=utf-8");
        //用以解决中文不能显示出来的问题 
        $file_name=iconv("utf-8","gb2312",$file_name); 
        $file_sub_path='./Public/';
        $file_path=$file_sub_path.$file_name; 
        //首先要判断给定的文件存在与否 
        if(!file_exists( $file_path ) ){ 
            echo "没有该文件文件"; 
            die ; 
            } 
        $fp = fopen($file_path , "r"); 
        $file_size = filesize( $file_path );
        Header("Content-type: application/octet-stream"); 
        Header("Accept-Ranges: bytes"); 
        Header("Accept-Length:".$file_size); 
        Header("Content-Disposition: attachment; filename=".$file_name); 
        $buffer=1024; 
        $file_count=0; 
        //向浏览器返回数据 
        while(!feof($fp) && $file_count<$file_size){ 
            $file_con=fread($fp,$buffer); 
            $file_count+=$buffer; 
            echo $file_con;
        } 
        fclose($fp);
     }

	/**
	 * mvp投票开关
	 *@param matchid
	 */
	public function mvpSupportOff(){
		$matchid = I('matchid');
		$matchleagueid = I('matchleagueid');
		$type = I('type');
		$res = M('match')->where("matchid=$matchid")->getField('mvpsupportoff');
		$res = M('match')->where("matchid=$matchid")->save(array('mvpsupportoff'=>($res+1)%2));
		if ($res != 0) {
			redirect(U('Home/Match/matchList',array('matchleagueid'=>$matchleagueid,'type'=>$type)));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

	/**
	 * ajax获取省，市
	 *@param pid
	 */
	public function ajaxGetP(){
		$pid = intval(I('pid'));
		$res = M('area')->where("pid=$pid")->select();
		echo jsonFormat(200,$res);
	}

	/**
	 * ajax添加省市
	 *@param pid
	 *@param name
	 */
	public function ajaxAddP(){
		$pid = intval(I('pid'));
		$name = I('name');
		$model =M('area');
		$count = $model->where("pid=$pid and name='$name'")->count();
		if ($count != 0) {
			echo jsonFormat(302,"已存在");die;
		}
		$data['pid'] = $pid;
		$data['name'] = $name;
		$res = M('area')->add($data);
		if ($res !=0) {
			echo jsonFormat(200,$res);die;
		}else{
			echo jsonFormat(300,"失败");die;
		}
	}


	/**
	 * 联赛新闻列表
	 *@param matchleagueid
	 */
	public function newsList(){
		$matchleagueid = I('matchleagueid');
		$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('news');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->where('matchleagueid='.$matchleagueid)->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
    	$res = M('news')->where('matchleagueid='.$matchleagueid)->limit($Page->firstRow.','.$Page->listRows)->select();
    	foreach ($res as $key => $value) {
    		$res[$key]['commentcount'] = M('newscomment')->where('newsid='.$value['newsid'])->count();
    	}
    	$this->res = $res;
    	$this->page = $show;
    	$this->display();
	}
}


 ?>