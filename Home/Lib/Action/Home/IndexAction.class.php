<?php
// 用户中心
class IndexAction extends CommonAction {
	/**
	 * 用户列表
	 */
    public function index(){
    	$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('user');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
    	$res = D('User')->relation('userdata')->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->res = $res;
    	$this->page = $show;
    	$this->display();
    	// var_dump($res);die;
    }

    /**
     * 封停或者解封用户
     */
    public function denyUser(){
    	$userid = I('uid',intval);
    	$isdeny = M('user')->where("uid=$userid")->getField('isdeny');
    	$res = M('user')->where("uid=$userid")->save(array('isdeny'=>($isdeny+1)%2));
    	if ($res!=0) {
    		redirect(U('Home/Index/index'));
    	}else{
    		echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
    	}
    }

    /**
     * 管理员列表
     */
    public function adminList(){
    	$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('admin');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
    	$userData = $model->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->res = $userData;
    	$this->page = $show;
		$this->display();
    }

    /**
     * 删除管理员
     */
    public function delAdmin(){
    	$userid = I('adminid',intval);
        if ($userid == 1 ) {
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("不能删除admin"); </script>';die;
        }
    	$res = M('admin')->where("adminid=$userid")->delete();
    	if ($res!=0) {
    		redirect(U('Home/Index/admin'));
    	}else{
    		echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
    	}
    }

    /**
     * 添加管理员
     */
    public function addAdmin(){
    	$this->display();
    }

    /**
     * 添加管理员处理
     */
    public function addAdminHandle(){
    	$model = M('admin');
    	$data['username'] = I('username');
    	$data['userpass'] = md5(I('userpass'));
		$data['addtime'] = time();
		$res = $model->add($data);
		if($res!=0) {
			redirect(U('Index/adminList'));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
    }

        /**
     * 修改密码
     *@param adminid
     */
    public function chpw(){
        $this->display();
    }

    /**
     * 修改密码处理
     */
    public function chpwHandle(){
        $adminid = I('adminid');
        $oldpass = I('oldpass');
        $newpass = I('newpass');
        $newpasss = I('newpasss');
        if ($newpass != $newpasss) {
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("两次输入不一致"); </script>';die;
        }
        $pass = M('admin')->where("adminid=$adminid")->getField('userpass');
        if ($pass != md5($oldpass)) {
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("密码错误"); </script>';die;
        }
        $res = M('admin')->where("adminid=$adminid")->save(array('userpass'=>md5($newpass)));
        if ($res!=0) {
            redirect(U('/Home/Index/adminList'));
        }else{
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';die;
        } 
    }

    /**
     * 个性签名列表
     */
    public function signatureList(){
        $num = I('num',intval)==0?20:I('num',intval);
        $model = M('signature');
        import('ORG.Util.Page');// 导入分页类
        $count      = $model->count();// 查询满足要求的总记录数
        $Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
        $Page->setConfig('prev','上一页');
        $Page->setConfig('next','下一页');
        $show       = $Page->show();
        $res  = M('signature')->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->page = $show;
        $this->res = $res;
        $this->display();
    } 

    /**
     * 添加个性签名
     */
    public function addSignature(){
        $this->display();
    }

    /**
     * 添加个性签名处理
     */
    public function addSignatureHandle(){
        $signature = I('signature');
        if ($signature=='') {
           redirect('Home/Index?signatureList');
        }else{
            $data['signature'] = $signature;
            $data['addtime'] = time();
            $res = M('signature')->add($data);
            if($res!=0) {
                redirect(U('Index/signatureList'));
            }else{
                echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
            }
        }
    }

    /**
     * 删除个性签名
     *@param id
     */
    public function delSignature(){
        $id = I('id',intval);
        $res = M('signature')->where('id='.$id)->delete();
        if($res!=0) {
            redirect(U('Index/signatureList'));
        }else{
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
        }
    }


    /**
     * 启动页面管理
     */
    public function startPageList(){
        $res = M('startpage')->order('addtime DESC')->select();
        foreach ($res as $key => $value) {
            $res[$key]['img'] = httpImg($value['img']);
        }
        $this->res = $res;
        $this->display();
    }



    /**
     * 添加启动页
     */
    public function addStartPage(){
        $this->display();
    }

    /**
     * 添加启动页处理
     */
    public function addStartPageHandle(){
        $_POST['img'] = uploadFormImg('img',C('startpageImg'));
        $model = D('Startpage');
        if (!$model->create()){
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
        }else{
            $res = $model->add();
            if ($res != 0) {
                redirect(U('Home/Index/startPageList'));
            }else{
                echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
            }
        }
    }

    /**
     * 删除启动页
     */
    public function delStartPage(){
        $spid = I('spid');
        $res = M('startpage')->where("spid=$spid")->delete();
        if ($res!=0) {
            redirect(U('Home/Index/startPageList'));
        }else{
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
        }
    }

    /**
     * 意见反馈
     */
    public function faq(){
        $num = I('num',intval)==0?20:I('num',intval);
        $model = M('faq');
        import('ORG.Util.Page');// 导入分页类
        $count      = $model->count();// 查询满足要求的总记录数
        $Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
        $Page->setConfig('prev','上一页');
        $Page->setConfig('next','下一页');
        $show       = $Page->show();
        $res  = M('faq')->join('left join userdata on userdata.uid=faq.uid')->field('faq.*,userdata.nickname')->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->page = $show;
        $this->res = $res;
        $this->display();
    }

    /**
     * 删除意见反馈
     */
    public function delFaq(){
        $faqid = I('faqid');
        $res = M('faq')->where("faqid=$faqid")->delete();
        if ($res!=0) {
            redirect(U('Home/Index/faq'));
        }else{
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
        }
    }

    /**
     * 版本列表
     */
    public function version(){
        $num = I('num',intval)==0?20:I('num',intval);
        $model = M('version');
        import('ORG.Util.Page');// 导入分页类
        $count      = $model->count();// 查询满足要求的总记录数
        $Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
        $Page->setConfig('prev','上一页');
        $Page->setConfig('next','下一页');
        $show       = $Page->show();
        $res  = M('version')->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->page = $show;
        $this->res = $res;
        $this->display();
    }

    /**
     * 添加新版本处理
     */
    public function addVersionHandle(){
        // var_dump($_FILES);die;
        $_POST['path'] = uploadFormFile('path',C('version'));
        $model = D('Version');
        if (!$model->create()){
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
        }else{
            $res = $model->add();
            if ($res != 0) {
                redirect(U('Home/Index/version'));
            }else{
                echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
            }
        }
    }
    /**
     * 添加版本显示
     */
    public function addVersion(){
        $this->display();
    }

    /**
     * 删除版本
     */
    public function delVersion(){
        $versionNo = I('versionNo');
        $res = M('version')->where("versionNo=$versionNo")->delete();
        if ($res!=0) {
            redirect(U('Home/Index/version'));
        }else{
            echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
        }
    }

}