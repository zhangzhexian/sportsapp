<?php 
/**
* 约伙伴模块
*/
class YhbAction extends CommonAction
{
	
	/**
	 * 约伙伴列表
	 */
	public function yhbList(){
		$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('yhb');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
		$res = M('yhb')->limit($Page->firstRow.','.$Page->listRows)->select();
		foreach ($res as $key => $value) {
			$res[$key]['nickname'] = formatUser($value['uid'])['nickname'];
			$res[$key]['xycount'] = M('yhbxy')->where("yhbid=".$value['yhbid'])->count();
			$res[$key]['plcount'] = M('yhbpl')->where("yhbid=".$value['yhbid'])->count();
		}
		$this->page = $show;
		$this->res = $res;
		$this->display();
	}

	/**
	 * 约伙伴响应列表
	 *@param yhbid
	 */
	public function yhbxyList(){
		$yhbid = I('yhbid');
		$res = M('yhbxy')->where('yhbid='.$yhbid)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['sex'] = $userData['sex'];
		}
		$this->res = $res;
		$this->display();
	}

	/**
	 * 约伙伴评论列表
	 *@param yhbid
	 */
	public function yhbplList(){
		$yhbid = I('yhbid');
		$res = M('yhbpl')->where('yhbid='.$yhbid)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['sex'] = $userData['sex'];
			$res[$key]['hfcount'] = M('hfpl')->where("yhbplid=".$value['yhbplid'])->count();
		}
		$this->res = $res;
		$this->display();
	}

	/**
	 * 约伙伴评论回复列表
	 *@param yhbplid
	 */
	public function hfplList(){
		$yhbplid = I('yhbplid');
		$model = new Model();
		$res = $model->query("select hf.*,ud1.nickname as nickname,ud2.nickname as hfnickname from hfpl hf left join userdata ud1 on hf.uid=ud1.uid left join userdata ud2 on hf.hfuid=ud2.uid where hf.yhbplid=$yhbplid order by addtime asc");
		// echo $model->getDbError();
		$this->res = $res;
		$this->display();
		// var_dump($res);
	}

	/**
	 * 删除约伙伴信息
	 *@param yhbid
	 */
	public function delYhb(){
		$yhbid = I('yhbid');
		$res = M('yhb')->where("yhbid=$yhbid")->delete();
		if ($res!=0) {
    		redirect(U('Home/Yhb/yhbList'));
    	}else{
    		echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
    	}
	}

	/**
	 * 删除约伙伴评论
	 *@param yhbplid
	 *@param yhbid
	 */
	public function delYhbpl(){
		$yhbid = I('yhbid');
		$yhbplid = I('yhbplid');
		$res = M('yhbpl')->where("yhbplid=$yhbplid")->delete();
		if ($res!=0) {
    		redirect(U('Home/Yhb/yhbplList',array('yhbid'=>$yhbid)));
    	}else{
    		echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
    	}
	}

	/**
	 * 删除约伙伴评论回复
	 *@param yhbplid
	 *@param hfplid
	 */
	public function delHfpl(){
		$hfplid = I('hfplid');
		$yhbplid = I('yhbplid');
		$res = M('hfpl')->where("hfplid=$hfplid")->delete();
		if ($res!=0) {
    		redirect(U('Home/Yhb/HfplList',array('yhbplid'=>$yhbplid)));
    	}else{
    		echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
    	}
	}

}



?>