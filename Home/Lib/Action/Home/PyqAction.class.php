<?php 
/**
* 朋友圈模块
*/
class PyqAction extends CommonAction
{
	
	/**
	 * 朋友圈消息列表
	 */
	public function pyqList(){
		$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('pyq');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
		$res = M('pyq')->order('pyqid DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		foreach ($res as $key => $value) {
			$res[$key]['nickname'] = M('userdata')->where("uid=".$value['uid'])->getField('nickname');
			$res[$key]['commentcount'] = M('pyqpl')->where("pyqid=".$value['pyqid'])->count();
			$res[$key]['zhan'] = M('pyqzhan')->where("pyqid=".$value['pyqid'])->count();
			$res[$key]['img'] = M('pyqimg')->where("pyqid=".$value['pyqid'])->select();
		}
		$this->page = $show;
		$this->res = $res;
		$this->display();
	}

	/**
	 * 朋友圈评论列表
	 *@param pyqid
	 */
	public function commentList(){
		$pyqid = I('pyqid');
		$res = M('pyqpl')->where('pyqid='.$pyqid)->select();
		$res = is_null($res)?array():$res;
		foreach ($res as $key => $value) {
			$userData = formatUser($value['uid']);
			$res[$key]['nickname'] = $userData['nickname'];
			$res[$key]['sex'] = $userData['sex'];
			$res[$key]['hfcount'] = M('pyqplhf')->where("pyqplid=".$value['pyqplid'])->count();
		}
		$this->res = $res;
		$this->display();
	}

	/**
	 * 约伙伴评论回复列表
	 *@param pyqplid 
	 */
	public function hfplList(){
		$pyqplid = I('pyqplid');
		$model = new Model();
		$res = $model->query("select hf.*,ud1.nickname as nickname,ud2.nickname as hfnickname from pyqplhf hf left join userdata ud1 on hf.uid=ud1.uid left join userdata ud2 on hf.hfuid=ud2.uid where hf.pyqplid=$pyqplid order by pyqplhfid asc");
		// echo $model->getDbError();
		$this->res = $res;
		$this->display();
		// var_dump($res);
	}

	/**
	 * 删除朋友圈消息
	 *@param pyqid
	 */
	public function delPyq(){
		$pyqid = I('pyqid',intval);
		$res = M('pyq')->where("pyqid=$pyqid")->delete();
		if(0 != $res){
			redirect(U('Home/Pyq/pyqList'));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

	/**
	 * 删除朋友圈评论消息
	 *@param pyqid
	 *@param pyqplid
	 */
	public function delPyqPl(){
		$pyqid = I('pyqid',intval);
		$pyqplid = I('pyqplid',intval);
		$res = M('pyqpl')->where("pyqplid=$pyqplid")->delete();
		if(0 != $res){
			redirect(U('Home/Pyq/commentList',array('pyqid'=>$pyqid)));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

	/**
	 * 删除朋友圈评论回复
	 *@param pyqplid
	 *@param pyqplhfid
	 */
	public function delPyqplhf(){
		$pyqplhfid = I('pyqplhfid',intval);
		$pyqplid = I('pyqplid',intval);
		$res = M('pyqplhf')->where("pyqplhfid=$pyqplhfid")->delete();
		if(0 != $res){
			redirect(U('Home/Pyq/hfplList',array('pyqplid'=>$pyqplid)));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}

}


 ?>