<?php 

/**
* 约场地控制器
*/
class YcdAction extends CommonAction
{
	
	/**
	 * 约场地列表
	 */
	public function ycdList(){
		$res = M('ycd')->where('isdelete=0')->select();
		foreach ($res as $key => $value) {
			$res[$key]['img'] = httpImg(explode(',', $value['img'])[0]);
		}
		$this->res = $res;
		$this->display();
	}

	/**
	 * 删除场地
	 *@param ycdid
	 */
	public function delYcd(){
		$ycdid = I('ycdid');
		$res = M('ycd')->where("ycdid=$ycdid")->save(array("isdelete"=>1));
		if ($res!=0) {
			redirect(U('Home/Ycd/ycdList'));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("失败"); </script>';
		}
	}
}


 ?>