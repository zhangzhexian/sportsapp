<?php 

/**
* 新闻模块
*/
class NewsAction extends CommonAction
{
	/**
	 * 新闻列表
	 */
	public function newsList(){
		$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('news');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->where('matchleagueid=0')->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
    	$res = M('news')->where('matchleagueid=0')->limit($Page->firstRow.','.$Page->listRows)->select();
    	foreach ($res as $key => $value) {
    		$res[$key]['commentcount'] = M('newscomment')->where('newsid='.$value['newsid'])->count();
    	}
    	$this->res = $res;
    	$this->page = $show;
    	$this->display();
	}

	/**
	 * 添加新闻
	 */
	public function addNews(){
		$this->display();
	}

	public function addNewsHandle(){
		$_POST['newsimg'] = uploadFormImg('newsimg',C('newsImg'));
		$model = D('News');
		if (!$model->create()) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
		}else{
			$res = $model->add();
			if ($res != 0) {
				redirect(U('Home/News/newsList'));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
			}
		}
	}

	/**
	 * 删除新闻
	 */
	public function delNews(){
		$newsid = I('newsid');
		$res = M('news')->where('newsid='.$newsid)->delete();
		if ($res != 0) {
			redirect(U('Home/News/newsList'));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
		}
	}
	/**
	 * 评论列表
	 */
	public function commentList(){
		$newsid = I('newsid');
		$num = I('num',intval)==0?20:I('num',intval);
    	$model = M('newscomment');
    	import('ORG.Util.Page');// 导入分页类
		$count      = $model->where("newsid=$newsid")->count();// 查询满足要求的总记录数
		$Page       = new Page($count,$num);// 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('prev','上一页');
		$Page->setConfig('next','下一页');
		$show       = $Page->show();// 分页显示输出
    	$res = M('newscomment nc')->join('left join userdata ud on nc.uid=ud.uid')->field('nc.*,ud.nickname')->where("newsid=$newsid")->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->res = $res;
    	$this->page = $show;
    	$this->display();
	}
	/**
	 * 删除评论
	 */
	public function delComent(){
		$newscommentid = I('newscommentid');
		$newsid = I('newsid');
		$res = M('newscomment')->where("newscommentid=$newscommentid")->delete();
		if ($res != 0) {
			redirect(U('Home/News/commentList',array('newsid'=>$newsid)));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
		}
	}


	/**
	 * 新闻编辑
	 *@param newsid
	 */
	public function editNews(){
		$newsid = I('newsid');
		$res = M('news')->where("newsid=$newsid")->find();
		$this->res = $res;
		$this->display();
	}

	/**
	 * 新闻编辑处理
	 *@param mixed
	 */
	public function editNewsHandle(){
		$newsid = $_POST['newsid'];
		if ($_FILES['newsimg']['name']!='') {
			$_POST['newsimg'] = uploadFormImg('newsimg',C('newsImg'));
		}else{
			unset($_POST['newsimg']);
		}
		$model = M('news');
		if (!$model->create()) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
		}else{
			$res = $model->where("newsid=".$newsid)->save($_POST);
			if ($res != 0) {
				redirect(U('Home/News/newsList'));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
			}
		}
	}

	/**
	 * banner列表
	 */
	public function bannerList(){
		$res = M('banner')->order('sort')->select();
		$this->res = $res;
		$this->display();
	}

	/**
	 * 添加banner
	 */
	public function addBanner(){
		$this->display();
	}

	/**
	 *添加banner处理 
	 */
	public function addBannerHandle(){
		if ($_FILES['img']['name']!='') {
			$_POST['img'] = uploadFormImg('img',C('newsImg'));
		}else{
			unset($_POST['img']);
		}
		$model = D('Banner');
		if (!$model->create()) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
		}else{
			$res = $model->add();
			if ($res != 0) {
				redirect(U('Home/News/bannerList'));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
			}
		}
	}

	/**
	 * 广告列表
	 */
	public function adList(){
		$res = M('ad')->field('adid,title')->order('addtime DESC')->select();
		$this->res = $res;
		$this->display();
	}

	/**
	 * 添加广告
	 */
	public function addAd(){
		$this->display();
	}

	/**
	 * 添加广告处理
	 *@param title
	 *@param content
	 */
	public function addAdHandle(){
		$model = D('Ad');
		if (!$model->create()) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
		}else{
			$res = $model->add();
			if ($res != 0) {
				redirect(U('Home/News/adList'));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
			}
		}
	}

	/**
	 * 编辑广告
	 */
	public function editAd(){
		$adid = I('adid',intval);
		$res = M('ad')->where("adid=$adid")->find();
		$this->res = $res;
		$this->display();
	}

	/**
	 * 编辑广告处理
	 */
	public function editAdHandle(){
		$model = D('Ad');
		if (!$model->create()) {
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);;alert("'.$model->getError().'"); </script>';die;
		}else{
			$res = $model->save();
			if ($res != 0) {
				redirect(U('Home/News/adList'));
			}else{
				echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
			}
		}
	}

	/**
	 * 删除广告
	 *@param adid
	 */
	public function delAd(){
		$adid = I('adid',intval);
		$res = M('ad')->where("adid=$adid")->delete();
		if ($res != 0) {
			redirect(U('Home/News/adList'));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
		}
	}

	/**
	 * ajax获取banner连接内容(新闻或者广告)
	 *@param type 0广告 1新闻
	 */
	public function ajaxGetList(){
		$type = I('type',intval);
		if ($type == 0) {
			$res = M('ad')->field('adid as linkid,title')->select();
		}else{
			$res = M('news')->where("matchleagueid=0")->field('newsid as linkid,newstitle as title')->select();
		}
		$res = empty($res)?array(array('linkid'=>'err','title'=>'--没有内容--')):$res;
		echo jsonFormat(200,$res);		
	}



	/**
	 * 删除banner
	 *@param bannerid
	 */
	public function delBanner(){
		$bannerid  = I('bannerid');
		$res = M('banner')->where("bannerid=$bannerid")->delete();
		if ($res != 0) {
			redirect(U('Home/News/bannerList'));
		}else{
			echo '<script  language="javascript" type="text/javascript">window.history.back(-1);alert("失败"); </script>';
		}
	}
}


 ?>