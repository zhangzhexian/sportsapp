<?php
return array(
	//'配置项'=>'配置值'
	'APP_GROUP_LIST' => 'Home,Api', //项目分组设定
	'DEFAULT_GROUP'  => 'Home', //默认分组
	'URL_CASE_INSENSITIVE' =>true,//大小写
	'DB_TYPE'=> 'mysql',          // 数据库类型
    'DB_HOST'=> '127.0.0.1', // 数据库服务器地址
    'DB_NAME' =>'yundong', //数据库名称
    'DB_USER'=>'root', // 数据库用户名
    'DB_PWD'=>'', // 数据库密码
    'DB_PORT'=>'3306', // 数据库端口
    'DB_PREFIX' =>null,
    'DB_CHARSET'=>'UTF8',  //数据库编码

);
?>