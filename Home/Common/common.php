<?php 

/**
 * 接口数据返回
 *@param code
 *@param msg
 */
 function get_api_result($status, $msg){
            if($status){
                echo json_encode(array('code'=>$status,'msg'=>$msg));die;
            }else{
                echo json_encode(array('code'=>'Err','msg'=>'status 不存在'));die;
            }
    } 
/**
 * 递归创建目录
 */
function makeDir($str){
        $path = explode('/', $str);
        $j=count($path);
        $newStr='.';
        for ($i=1; $i <$j-1 ; $i++) { 
             $newStr = $newStr.'/'.$path[$i];
        }
        if(file_exists($newStr)){
        	mkdir($str);
        }else{
       		makeDir($newStr);
        	mkdir($str);
        }
       
}

/**
 * base64图片上传
 *@param $img
 *@param $path
 *@return imgfullpath
 */
function uploadBase64Img($img,$path){
	if (file_exists($path)&&is_dir($path)){
		1==1;
	}else{
		makeDir('.'.$path);
	}
	$fileName = time().mt_rand(10000,99999).".jpg";
	$img = base64_decode($img);
	$flag = file_put_contents('.'.$path.'/'.$fileName, $img);
	if ($flag) {
		return $path.'/'.$fileName;
	}else{
		zlog("图片上传失败！$path/$fileName;");
	}
}


/**
 * 写日志
 */
function zlog($msg)
{
    $log = date("Y-m-d H:i:s",time()).':   ';
    $log .= 'message:'.$msg;
    $path = './log/'.date('Ymd',time());
    file_exists($path)&&is_dir($path)?0==0:makeDir($path);
    $fh = fopen($path."/log.txt", 'a');
    fwrite($fh, $log."\r\n");
    fclose($fh);
}


/**
 * 图片转换全路径
 */
function httpImg($path){
        if ($path == '') {
           return '';
        }
		return $path==''?'':C('host').$path;
}

/**
 * 检测用户是否纯在
 */
function checkUser($uid,$bool=false){
        $count = M('user')->where("uid=$uid")->count();
        if ($count==0) {
            if (!$bool) {
                zlog('用户'.$uid.'不存在');
               get_api_result(309,'用户'.$uid.'不存在');
            }else{
                return false;
            }
            
        }else{
            return $uid;
        }
}
/**
 * 后台json格式化输出
 */
function jsonFormat($status , $data){
        return json_encode(array('code'=>$status,'data'=>$data));
}


/**
 * 
 * @param unknown_type $zippath 压缩包存放路径
 * @param unknown_type $picpath 图片存放路径
 * @return array|boolean upload 上传的压缩包zip
 */
function unzip($zippath,$picpath)
{
    $time = time ().rand(1000, 9999);
    $path = '.'.$zippath.'/'.$time.".zip";
    if(isset($_FILES["upload"]))
    {
        if(!(file_exists($path)&&is_dir($path))){
            makeDir(dirname($path));
        }
        move_uploaded_file($_FILES["upload"]["tmp_name"], $path);
        $listname = '.'.$picpath.'/'.$time;
        $zip = new ZipArchive() ;
        if ($zip->open($path) !== TRUE) {
            die ('Could not open archive');
        }
        makeDir($listname);
        $zip->extractTo($listname);
        $zip->close();
        $filename = getImg($listname);
        return $filename;
    }else
    {
        return false;
    }

}

function getImg($path,$arr=array()) {
    $current_dir = opendir($path);    //opendir()返回一个目录句柄,失败返回false
    while(($file = readdir($current_dir)) !== false) {    //readdir()返回打开目录句柄中的一个条目
            if ($file=='.'||$file=='..') {
                continue;
            }
            if (is_dir($path.'/'.$file)) {
                $arr = getImg($path.'/'.$file,$arr);
                continue;
            }
            $arr[] =  substr($path.'/'.$file, 1);
    }
    return $arr;
}

/**
 * 格式化用户信息
 */
function formatUser($uid){
    if (!checkUser($uid)) {
       
       return false;
    }
    $res =  M('userdata')->where('uid='.$uid)->field('nickname,headimg,postion,sex,borndate')->find();
    $data['nickname'] = $res['nickname'];
    $data['postion'] = $res['postion'];
    $data['sex'] = $res['sex'];
    $data['borndate'] = $res['borndate'];
    $data['headimg'] = httpImg($res['headimg']);
    return $data;
}


/**
 * 上传表单图片
 *@param key
 *@param path
 */
function uploadFormImg($key,$path){
    if (!file_exists($path)||!is_dir($path)){
        makeDir('.'.$path);
    }
    $filename = time ().rand(1000, 9999) . ".jpg";
    if ($_FILES[$key]['tmp_name']!='') {         
        if(move_uploaded_file($_FILES[$key]['tmp_name'], '.'.$path.'/'.$filename)){
            return $path.'/'.$filename;
        }else{
            zlog('单图上传失败:'.$path.'/'.$filename);
            return "单图上传失败";
        }
    }
    return "";
}

// /**
//  * 字符串替换补全图片路径
//  *@param String img
//  *@param String img
//  */
// function imgStrReplace($img){
// return str_replace('/'.C('appname').'/Public', C('host').'/Public', $img);
// }

/**
 * 字符串替换补全图片路径
 *@param String img
 *@param String img
 */
function imgStrReplace($img){
return str_replace('/Public', C('host').'/Public', $img);
}

/**
 * 格式化球队队长信息
 *@param teamid
 */
function formatTeamLeader($teamid){
    $playerData = M('teamuser')->join('left join userdata on teamuser.uid=userdata.uid')->where("teamid=".$teamid." and role in (1,2) and isthrough=1")->field('userdata.sex,userdata.uid,userdata.nickname,userdata.headimg,teamuser.role')->select();
    foreach ($playerData as $k => $v) {
        $playerData[$k]['headimg'] = httpImg($v['headimg']);
        $playerData[$k]['tel'] = M('user')->where("uid=".$v['uid'])->getField('tel');
    }
    return $playerData;
}

 ?>